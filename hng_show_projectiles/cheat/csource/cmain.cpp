#include <stdio.h>
#include <windows.h>

#include "../cheader/csummoner.h"

const unsigned long OFFSET_TERMINAL = 0x5C520;
const unsigned long OFFSET_SHOW_PROJECTILES = 0x118;
const unsigned long OFFSET_CLEAR_ALL_DEBUG = 0x394;

void main() {
  unsigned long terminal;
  unsigned long pid;
  unsigned long globalsDll = 0;
  unsigned long playerDll = 0;
  unsigned char showProjectiles = 0;
  
  printf("||=======================================================||\n");
  printf("||          cheat for hng version 150912 QQ              ||\n");
  printf("||                 show projectiles                      ||\n");
  printf("|| made by: lava                                         ||\n");
  printf("||=======================================================||\n");
  
  printf("waiting for hng\n");
  pid = get_proc(L"hng.exe");
  

  printf("searching modules ... ");
  keGetProcessModule(pid, (unsigned long)&globalsDll, L"globals.dll");

  if (globalsDll != 0) {
    printf("| Done\n");
    printf("enabling show projectile path ... ");

    keReadVirtualMemory(pid, globalsDll + OFFSET_TERMINAL,
                        (unsigned long)&terminal, 4);

    keWriteVirtualMemory(pid, terminal + OFFSET_SHOW_PROJECTILES,
                         (unsigned long)"\x01", 1);

    keWriteProtectedMemory(pid, 0x0006E694,
                           (unsigned long)"\x08\x00\x00\x00", 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
    
    keReadVirtualMemory(pid, terminal + OFFSET_SHOW_PROJECTILES,
                        (unsigned long)&showProjectiles, 1);

    if (showProjectiles != 0) {
      printf("| Done\n");
      printf("wokring to clear paths every 400ms ... ");

      for (; pid != 0; Sleep(400)) {
        pid = get_proc_light("hng.exe");

        if (pid != 0) {
          keWriteVirtualMemory(pid, terminal + OFFSET_CLEAR_ALL_DEBUG,
                               (unsigned long)"\x01", 1);
        }
      }
      printf("\nseems like game ended\n");
    } else {
      printf("| Failed\n");
    }
  } else {
    printf("| Failed\n");
  }

  system("pause");
}
