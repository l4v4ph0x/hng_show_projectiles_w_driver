#include "../cheader/csummoner.h"
#include <windows.h>

typedef struct _INOUT_PARAM {
  ULONG Req;
  ULONG Param1;
  ULONG Param2;
  ULONG Param3;
  ULONG Param4;
  ULONG Param5;
  CHAR Param6[256];
  ULONG Param7;
  ULONG Param8;
} INOUT_PARAM, *PINOUT_PARAM;

#define DRIVER_NAME "\\\\.\\LDSP6"

#define DUMMYDRV_REQUEST1                                                      \
  CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0820, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

void keReadVirtualMemory(unsigned long targetPid, unsigned long targetAddress,
                         unsigned long sourceAddress, unsigned long size) {
  HANDLE h;
  INOUT_PARAM tmp;
  DWORD bytesIO;

  h = CreateFile(TEXT(DRIVER_NAME), GENERIC_READ | GENERIC_WRITE,
                 FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0,
                 NULL);
  if (h != INVALID_HANDLE_VALUE) {
    tmp.Req = 1;
    tmp.Param1 = targetPid;
    tmp.Param2 = GetCurrentProcessId();
    tmp.Param3 = targetAddress;
    tmp.Param4 = sourceAddress;
    tmp.Param5 = size;

    DeviceIoControl(h, DUMMYDRV_REQUEST1, &tmp, sizeof(tmp), &tmp, sizeof(tmp),
                    &bytesIO, NULL);

    CloseHandle(h);
  }
}

void keWriteVirtualMemory(unsigned long targetPid, unsigned long targetAddress,
                          unsigned long sourceAddress, unsigned long size) {
  HANDLE h;
  INOUT_PARAM tmp;
  DWORD bytesIO;

  h = CreateFile(TEXT(DRIVER_NAME), GENERIC_READ | GENERIC_WRITE,
                 FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0,
                 NULL);
  if (h != INVALID_HANDLE_VALUE) {
    tmp.Req = 1;
    tmp.Param1 = GetCurrentProcessId();
    tmp.Param2 = targetPid;
    tmp.Param3 = sourceAddress;
    tmp.Param4 = targetAddress;
    tmp.Param5 = size;

    DeviceIoControl(h, DUMMYDRV_REQUEST1, &tmp, sizeof(tmp), &tmp, sizeof(tmp),
                    &bytesIO, NULL);

    CloseHandle(h);
  }
}

void keGetProcessModule(unsigned long targetPid, unsigned long sourceAddress,
                        const wchar_t *name) {
  HANDLE h;
  INOUT_PARAM tmp;
  DWORD bytesIO;

  *(unsigned long *)sourceAddress = 0;

  h = CreateFile(TEXT(DRIVER_NAME), GENERIC_READ | GENERIC_WRITE,
                 FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0,
                 NULL);
  if (h != INVALID_HANDLE_VALUE) {
    tmp.Req = 2;
    tmp.Param1 = targetPid;
    tmp.Param2 = GetCurrentProcessId();
    tmp.Param4 = sourceAddress;
    memcpy(tmp.Param6, name, 256);

    DeviceIoControl(h, DUMMYDRV_REQUEST1, &tmp, sizeof(tmp), &tmp, sizeof(tmp),
                    &bytesIO, NULL);

    CloseHandle(h);
  }
}

void keWriteProtectedMemory(unsigned long targetPid,
                            unsigned long targetAddress,
                            unsigned long sourceAddress, unsigned long size,
                            unsigned long protectionBeforeWrite,
                            unsigned long protectionAfterWrite) {
  HANDLE h;
  INOUT_PARAM tmp;
  DWORD bytesIO;

  h = CreateFile(TEXT(DRIVER_NAME), GENERIC_READ | GENERIC_WRITE,
                 FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0,
                 NULL);
  if (h != INVALID_HANDLE_VALUE) {
    tmp.Req = 3;
    tmp.Param1 = GetCurrentProcessId();
    tmp.Param2 = targetPid;
    tmp.Param3 = sourceAddress;
    tmp.Param4 = targetAddress;
    tmp.Param5 = size;
    tmp.Param7 = protectionBeforeWrite;
    tmp.Param8 = protectionAfterWrite;

    DeviceIoControl(h, DUMMYDRV_REQUEST1, &tmp, sizeof(tmp), &tmp, sizeof(tmp),
                    &bytesIO, NULL);

    CloseHandle(h);
  }
}
