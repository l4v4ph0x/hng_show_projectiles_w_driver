#pragma once

#include <ntddk.h>
#include <wdm.h>

DECLARE_HANDLE(HINSTANCE);
typedef HINSTANCE HMODULE;

typedef unsigned char BYTE;

typedef struct _KAPC_STATE {
  LIST_ENTRY ApcListHead[2];
  PKPROCESS Process;
  BOOLEAN KernelApcInProgress;
  BOOLEAN KernelApcPending;
  BOOLEAN UserApcPending;
} KAPC_STATE, *PKAPC_STATE;

typedef struct _PEB_LDR_DATA {
  BYTE Reserved1[8];
  PVOID Reserved2[3];
  LIST_ENTRY InMemoryOrderModuleList;
} PEB_LDR_DATA, *PPEB_LDR_DATA;

typedef struct _RTL_USER_PROCESS_PARAMETERS {
  BYTE Reserved1[16];
  PVOID Reserved2[10];
  UNICODE_STRING ImagePathName;
  UNICODE_STRING CommandLine;
} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;

typedef VOID(NTAPI *PPS_POST_PROCESS_INIT_ROUTINE)(VOID);

typedef struct _PEB {
  BYTE Reserved1[2];
  BYTE BeingDebugged;
  BYTE Reserved2[1];
  PVOID Reserved3[2];
  PPEB_LDR_DATA Ldr;
  PRTL_USER_PROCESS_PARAMETERS ProcessParameters;
  BYTE Reserved4[104];
  PVOID Reserved5[52];
  PPS_POST_PROCESS_INIT_ROUTINE PostProcessInitRoutine;
  BYTE Reserved6[128];
  PVOID Reserved7[1];
  ULONG SessionId;
} PEB, *PPEB;

typedef struct _PROCESS_BASIC_INFORMATION {
  PVOID Reserved1;
  PPEB PebBaseAddress;
  PVOID Reserved2[2];
  ULONG_PTR UniqueProcessId;
  PVOID Reserved3;
} PROCESS_BASIC_INFORMATION;
typedef PROCESS_BASIC_INFORMATION *PPROCESS_BASIC_INFORMATION;

typedef enum _PROCESSINFOCLASS {
  ProcessBasicInformation = 0,
  ProcessDebugPort = 7,
  ProcessWow64Information = 26,
  ProcessImageFileName = 27
} PROCESSINFOCLASS;

NTKERNELAPI VOID KeStackAttachProcess(IN PKPROCESS Process,
                                      OUT PKAPC_STATE ApcState);

NTKERNELAPI VOID KeUnstackDetachProcess(IN PKAPC_STATE ApcState);

NTKERNELAPI NTSTATUS ObOpenObjectByPointer(
    IN PVOID Object, IN ULONG HandleAttributes,
    IN PACCESS_STATE PassedAccessState OPTIONAL, IN ACCESS_MASK DesiredAccess,
    IN POBJECT_TYPE ObjectType OPTIONAL, IN KPROCESSOR_MODE AccessMode,
    OUT PHANDLE Handle);

NTKERNELAPI NTSTATUS PsLookupProcessByProcessId(IN HANDLE ProcessId,
                                                OUT PEPROCESS *Process);

NTKERNELAPI NTSTATUS ZwClose(IN HANDLE Handle);

NTSYSAPI NTSTATUS NTAPI ZwQueryInformationProcess(
    IN HANDLE ProcessHandle, IN PROCESSINFOCLASS ProcessInformationClass,
    OUT PVOID ProcessInformation, IN ULONG ProcessInformationLength,
    OUT PULONG ReturnLength OPTIONAL);

// not complete
typedef struct _LDR_DATA_TABLE_ENTRY {
  LIST_ENTRY InLoadOrderLinks;
  LIST_ENTRY InMemoryOrderLinks;
  LIST_ENTRY InInitializationOrderLinks;
  PVOID DllBase;
  PVOID EntryPoint;
  ULONG SizeOfImage;
  UNICODE_STRING FullDllName;
  UNICODE_STRING BaseDllName;
  ULONG Flags;
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;
