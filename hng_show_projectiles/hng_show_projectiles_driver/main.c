//#include "my.h"
#include "main.h"
#include <ntddk.h>
#include <wdm.h>

// https://projects.honeynet.org/svn/capture-hpc/capture-hpc/tags/2.5/capture-client/KernelDrivers/CaptureKernelDrivers/ProcessMonitor/CaptureProcessMonitor.c
// http://www.rdos.net/svn/tags/V9.2.5/watcom/bld/w32api/include/ddk/ntddk.mh
// https://blog.csdn.net/dasgk/article/details/17564033
// http://www.cppblog.com/newcoding/archive/2009/09/17/96563.aspx
// http://cppblog.com/sleepwom/archive/2009/09/17/96573.html
// https://github.com/Microsoft/Windows-driver-samples/blob/master/general/cancel/sys/cancel.c#L97

//#include "ntldr.h"
//#include "ntpebteb.h"

#define DEBUGPRINT

/*
 * PrintIrql
 *
 * Purpose:
 *
 * Debug print current irql.
 *
 */
VOID PrintIrql() {
  KIRQL Irql;
  PWSTR sIrql;

  Irql = KeGetCurrentIrql();

  switch (Irql) {

  case PASSIVE_LEVEL:
    sIrql = L"PASSIVE_LEVEL";
    break;
  case APC_LEVEL:
    sIrql = L"APC_LEVEL";
    break;
  case DISPATCH_LEVEL:
    sIrql = L"DISPATCH_LEVEL";
    break;
  case CMCI_LEVEL:
    sIrql = L"CMCI_LEVEL";
    break;
  case CLOCK_LEVEL:
    sIrql = L"CLOCK_LEVEL";
    break;
  case IPI_LEVEL:
    sIrql = L"IPI_LEVEL";
    break;
  case HIGH_LEVEL:
    sIrql = L"HIGH_LEVEL";
    break;
  default:
    sIrql = L"Unknown Value";
    break;
  }
  DbgPrint("KeGetCurrentIrql=%u(%ws)\n", Irql, sIrql);
}

VOID getProcessModule(PINOUTPARAM wp) {
  // param1: target proc
  // param2: source proc
  // param4: source proc address to write
  // param6: char arr [256] module name

  PEPROCESS targetProc = 0;
  PEPROCESS sourceProc = 0;
  PPEB32 pPeb32;
  SIZE_T Result;
  int statuss = 9;

  __try {
    PsLookupProcessByProcessId(wp->Param1, &targetProc);
    PsLookupProcessByProcessId(wp->Param2, &sourceProc);

    pPeb32 = (PPEB32)PsGetProcessWow64Process(targetProc);

    if (!pPeb32) {
      // statuss = 2;
    } else {
      // more extreme way
      // https://github.com/everdox/HIDInput/blob/master/input.h
      PRKAPC_STATE apc = NULL;

      KeStackAttachProcess(targetProc, &apc);
      ProbeForRead((PVOID)pPeb32->Ldr, 1, 4);

      if (!pPeb32->Ldr) {
        // statuss = 3;
      } else {
        PLIST_ENTRY32 pListEntry = (PLIST_ENTRY32)((PPEB_LDR_DATA32)pPeb32->Ldr)
                                       ->InLoadOrderModuleList.Flink;

        while (pListEntry !=
               &((PPEB_LDR_DATA32)pPeb32->Ldr)->InLoadOrderModuleList) {

          if (!pListEntry) {
            // statuss = 4;
            break;
          } else {
            PLDR_DATA_TABLE_ENTRY32 pEntry = CONTAINING_RECORD(
                pListEntry, LDR_DATA_TABLE_ENTRY32, InLoadOrderLinks);

            UNICODE_STRING ustr;
            UNICODE_STRING ustrcmp;

            RtlInitUnicodeString(&ustr, (PWCH)pEntry->BaseDllName.Buffer);
            RtlInitUnicodeString(&ustrcmp, wp->Param6);

            if (RtlCompareUnicodeString(&ustr, &ustrcmp, TRUE) == 0) {
              MmCopyVirtualMemory(PsGetCurrentProcess(), &pEntry->DllBase,
                                  sourceProc, wp->Param4, 4, KernelMode,
                                  &Result);
              break;
            }

            pListEntry = (PLIST_ENTRY32)pListEntry->Flink;
          }
        }
      }

      KeUnstackDetachProcess(&apc);
    }
  } __except (EXCEPTION_EXECUTE_HANDLER) {
  }
}

VOID getProcessModuleFullName(PINOUTPARAM wp) {
  // param1: target proc
  // param2: source proc
  // param4: source proc address to write
  // param6: char arr [256] module name

  PEPROCESS targetProc = 0;
  PEPROCESS sourceProc = 0;
  PPEB32 pPeb32;
  SIZE_T Result;
  int statuss = 9;

  __try {
    PsLookupProcessByProcessId(wp->Param1, &targetProc);
    PsLookupProcessByProcessId(wp->Param2, &sourceProc);

    pPeb32 = (PPEB32)PsGetProcessWow64Process(targetProc);

    if (!pPeb32) {
      // statuss = 2;
    } else {
      // more extreme way
      // https://github.com/everdox/HIDInput/blob/master/input.h
      PRKAPC_STATE apc = NULL;

      KeStackAttachProcess(targetProc, &apc);
      ProbeForRead((PVOID)pPeb32->Ldr, 1, 4);

      if (!pPeb32->Ldr) {
        // statuss = 3;
      } else {
        PLIST_ENTRY32 pListEntry = (PLIST_ENTRY32)((PPEB_LDR_DATA32)pPeb32->Ldr)
                                       ->InLoadOrderModuleList.Flink;

        while (pListEntry !=
               &((PPEB_LDR_DATA32)pPeb32->Ldr)->InLoadOrderModuleList) {

          if (!pListEntry) {
            // statuss = 4;
            break;
          } else {
            PLDR_DATA_TABLE_ENTRY32 pEntry = CONTAINING_RECORD(
                pListEntry, LDR_DATA_TABLE_ENTRY32, InLoadOrderLinks);

            UNICODE_STRING ustr;
            UNICODE_STRING ustrcmp;

            RtlInitUnicodeString(&ustr, (PWCH)pEntry->BaseDllName.Buffer);
            RtlInitUnicodeString(&ustrcmp, wp->Param6);

            if (RtlCompareUnicodeString(&ustr, &ustrcmp, TRUE) == 0) {
              char PtrFullPathFname[256];
              POBJECT_NAME_INFORMATION nameInfo = NULL;
              ANSI_STRING strVname;

              if (STATUS_SUCCESS == RtlUnicodeStringToAnsiString(&strVname, &pEntry->FullDllName,
                                               TRUE)) {
                strcpy(PtrFullPathFname, strVname.Buffer);
                RtlFreeAnsiString(&strVname);

                 MmCopyVirtualMemory(PsGetCurrentProcess(), &PtrFullPathFname,
                                    sourceProc, wp->Param4, 1024, KernelMode,
                                    &Result);
              }

             
              break;
            }

            pListEntry = (PLIST_ENTRY32)pListEntry->Flink;
          }
        }
      }

      KeUnstackDetachProcess(&apc);
    }
  } __except (EXCEPTION_EXECUTE_HANDLER) {
  }
}

VOID copyMemoryProtected(PINOUTPARAM wp) {
  // param1: target proc
  // param2: source proc
  // param4: source proc address to write

  PEPROCESS targetProc = 0;
  PEPROCESS sourceProc = 0;
  SIZE_T Result;
  //int statuss = 9;
  PRKAPC_STATE apc = NULL;

  __try {
    PVOID address = wp->Param4;
    //NTSTATUS status = 8;
    //ULONG valueToWrite = 8;

    PsLookupProcessByProcessId(wp->Param2, &targetProc);
    PsLookupProcessByProcessId(wp->Param1, &sourceProc);
    
    KeStackAttachProcess(targetProc, &apc);
    PMDL mdl = IoAllocateMdl(address, wp->Param5, FALSE, FALSE, NULL);

    MmProbeAndLockPages(mdl, KernelMode, IoReadAccess);
    address = MmGetSystemAddressForMdlSafe(mdl, HighPagePriority);

    MmProtectMdlSystemAddress(mdl, wp->Param7);
    if (MmIsAddressValid((void *)address)) {
      //RtlCopyMemory((void *)address, &valueToWrite, sizeof(valueToWrite));
      MmCopyVirtualMemory(sourceProc, wp->Param3,
                          PsGetCurrentProcess(),
                          address, wp->Param5, KernelMode, &Result);
      //statuss = 3;
    } else {
      //statuss = 2;
    }
    MmProtectMdlSystemAddress(mdl, wp->Param8);

    MmUnmapLockedPages(address, mdl);
    MmUnlockPages(mdl);
    IoFreeMdl(mdl);

    KeUnstackDetachProcess(&apc);
  } __except (EXCEPTION_EXECUTE_HANDLER) {
  }
}

VOID allocVirtualMemory(PINOUTPARAM wp) {
	// param2: target proc
	// param3: source proc address
	// param4: target proc address
	// param5: size to copy
	// param7: to protect before write
	// param8: to protect after write

	PEPROCESS targetProc = 0;
	PEPROCESS sourceProc = 0;
	SIZE_T Result;
	//int statuss = 9;
	PRKAPC_STATE apc = NULL;

	__try {
		PVOID address = wp->Param4;
		//NTSTATUS status = 8;
		//ULONG valueToWrite = 8;

		PsLookupProcessByProcessId(wp->Param2, &targetProc);
		PsLookupProcessByProcessId(wp->Param1, &sourceProc);

		KeStackAttachProcess(targetProc, &apc);
		PMDL mdl = IoAllocateMdl(address, wp->Param5, FALSE, FALSE, NULL);

		MmProbeAndLockPages(mdl, KernelMode, IoReadAccess);
		address = MmGetSystemAddressForMdlSafe(mdl, HighPagePriority);

		MmProtectMdlSystemAddress(mdl, wp->Param7);
		if (MmIsAddressValid((void*)address)) {
			//RtlCopyMemory((void *)address, &valueToWrite, sizeof(valueToWrite));
			MmCopyVirtualMemory(sourceProc, wp->Param3,
				PsGetCurrentProcess(),
				address, wp->Param5, KernelMode, &Result);
			//statuss = 3;
		}
		else {
			//statuss = 2;
		}
		MmProtectMdlSystemAddress(mdl, wp->Param8);

		MmUnmapLockedPages(address, mdl);
		MmUnlockPages(mdl);
		IoFreeMdl(mdl);

		KeUnstackDetachProcess(&apc);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
	}
}

/*
 * DevioctlDispatch
 *
 * Purpose:
 *
 * IRP_MJ_DEVICE_CONTROL dispatch.
 *
 */
NTSTATUS DevioctlDispatch(_In_ struct _DEVICE_OBJECT *DeviceObject,
                          _Inout_ struct _IRP *Irp) {
  NTSTATUS status = STATUS_SUCCESS;
  ULONG bytesIO = 0;
  PIO_STACK_LOCATION stack;
  BOOLEAN condition = FALSE;
  PINOUTPARAM rp, wp;

  UNREFERENCED_PARAMETER(DeviceObject);
  stack = IoGetCurrentIrpStackLocation(Irp);

  do {
    if (stack == NULL) {
      status = STATUS_INTERNAL_ERROR;
      break;
    }

    rp = (PINOUTPARAM)Irp->AssociatedIrp.SystemBuffer;
    wp = (PINOUTPARAM)Irp->AssociatedIrp.SystemBuffer;

    if (rp == NULL) {
      status = STATUS_INVALID_PARAMETER;
      break;
    }

    if (stack->Parameters.DeviceIoControl.IoControlCode == DUMMYDRV_REQUEST1) {
      // if (stack->Parameters.DeviceIoControl.InputBufferLength !=
      //    sizeof(INOUT_PARAM)) {
      //  status = STATUS_INVALID_PARAMETER;
      //  break;
      //}

      if (wp->Req == 1) {
        PEPROCESS proc1 = 0;
        PEPROCESS proc2 = 0;
        CHAR *localBuf = "\x21";
        SIZE_T Result;

        PsLookupProcessByProcessId(wp->Param1, &proc1);
        PsLookupProcessByProcessId(wp->Param2, &proc2);

        MmCopyVirtualMemory(proc1, wp->Param3, proc2, wp->Param4, wp->Param5,
                            KernelMode, &Result);

      } else if (wp->Req == 2) {
        // param1: target proc
        // param2: source proc
        // param4: source proc address to write
        // param6: char arr [256] module name

        getProcessModule(wp);
      } else if (wp->Req == 3) {
        // param1: target proc id
        // param3: target proc address
        // param4: page protection val
        // param5: size to protect

        copyMemoryProtected(wp);
      } else if (wp->Req == 4) {
        // param1: target proc
        // param2: source proc
        // param4: source proc address to write
        // param6: char arr [256] module name

        getProcessModuleFullName(wp);
      }

      wp->Req = 00000000;
      wp->Param1 = 11111111;
      wp->Param2 = 22222222;
      wp->Param3 = 33333333;
      wp->Param4 = 44444444;
      wp->Param5 = 55555555;
      wp->Param6[0] = 0;
      wp->Param7 = 77777777;
      wp->Param8 = 88888888;

      status = STATUS_SUCCESS;
      bytesIO = sizeof(INOUT_PARAM);
    } else {
      status = STATUS_INVALID_PARAMETER;
    }

  } while (condition);

  Irp->IoStatus.Status = status;
  Irp->IoStatus.Information = bytesIO;
  IoCompleteRequest(Irp, IO_NO_INCREMENT);
  return status;
}

/*
 * UnsupportedDispatch
 *
 * Purpose:
 *
 * Unused IRP_MJ_* dispatch.
 *
 */
NTSTATUS UnsupportedDispatch(_In_ struct _DEVICE_OBJECT *DeviceObject,
                             _Inout_ struct _IRP *Irp) {
  UNREFERENCED_PARAMETER(DeviceObject);

  Irp->IoStatus.Status = STATUS_NOT_SUPPORTED;
  IoCompleteRequest(Irp, IO_NO_INCREMENT);
  return Irp->IoStatus.Status;
}

/*
 * CreateDispatch
 *
 * Purpose:
 *
 * IRP_MJ_CREATE dispatch.
 *
 */
NTSTATUS CreateDispatch(_In_ struct _DEVICE_OBJECT *DeviceObject,
                        _Inout_ struct _IRP *Irp) {
  UNREFERENCED_PARAMETER(DeviceObject);

  IoCompleteRequest(Irp, IO_NO_INCREMENT);
  return Irp->IoStatus.Status;
}

/*
 * CloseDispatch
 *
 * Purpose:
 *
 * IRP_MJ_CLOSE dispatch.
 *
 */
NTSTATUS CloseDispatch(_In_ struct _DEVICE_OBJECT *DeviceObject,
                       _Inout_ struct _IRP *Irp) {
  UNREFERENCED_PARAMETER(DeviceObject);

  IoCompleteRequest(Irp, IO_NO_INCREMENT);
  return Irp->IoStatus.Status;
}

/*
 * DriverInitialize
 *
 * Purpose:
 *
 * Driver main.
 *
 */
NTSTATUS DriverInitialize(_In_ struct _DRIVER_OBJECT *DriverObject,
                          _In_ PUNICODE_STRING RegistryPath) {
  NTSTATUS status;
  UNICODE_STRING SymLink, DevName;
  PDEVICE_OBJECT devobj;
  ULONG t;

  // RegistryPath is NULL
  UNREFERENCED_PARAMETER(RegistryPath);

  RtlInitUnicodeString(&DevName, L"\\Device\\DBLS1");
  status = IoCreateDevice(DriverObject, 0, &DevName, FILE_DEVICE_UNKNOWN,
                          FILE_DEVICE_SECURE_OPEN, TRUE, &devobj);

  if (!NT_SUCCESS(status)) {
    return status;
  }

  RtlInitUnicodeString(&SymLink, L"\\DosDevices\\DBLS1");
  status = IoCreateSymbolicLink(&SymLink, &DevName);

  devobj->Flags |= DO_BUFFERED_IO;

  for (t = 0; t <= IRP_MJ_MAXIMUM_FUNCTION; t++)
    DriverObject->MajorFunction[t] = &UnsupportedDispatch;

  DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = &DevioctlDispatch;
  DriverObject->MajorFunction[IRP_MJ_CREATE] = &CreateDispatch;
  DriverObject->MajorFunction[IRP_MJ_CLOSE] = &CloseDispatch;
  DriverObject->DriverUnload =
      NULL; // nonstandard way of driver loading, no unload

  devobj->Flags &= ~DO_DEVICE_INITIALIZING;
  return status;
}

/*
 * DriverEntry
 *
 * Purpose:
 *
 * Driver base entry point.
 *
 */
NTSTATUS DriverEntry(_In_ struct _DRIVER_OBJECT *DriverObject,
                     _In_ PUNICODE_STRING RegistryPath) {
  NTSTATUS status;
  UNICODE_STRING drvName;

  /* This parameters are invalid due to nonstandard way of loading and should
   * not be used. */
  UNREFERENCED_PARAMETER(DriverObject);
  UNREFERENCED_PARAMETER(RegistryPath);

  //HANDLE threadHandle;
  //PsCreateSystemThread(&threadHandle, (ACCESS_MASK)GENERIC_ALL, NULL, (HANDLE)0,
  //                     NULL, protectMemory, &drvName);

  PrintIrql();

  RtlInitUnicodeString(&drvName, L"\\Driver\\DBLS1");
  status = IoCreateDriver(&drvName, &DriverInitialize);

  return status;
}