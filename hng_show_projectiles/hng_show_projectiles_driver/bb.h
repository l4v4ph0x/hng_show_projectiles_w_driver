#pragma once
#include <ntifs.h>
#include <windef.h>

// Pre-Processor definitions for our I/O control codes.
#define REMOVE_BEOBJECT_CALLBACKS_IOCTL                                        \
  CTL_CODE(FILE_DEVICE_KS, 0x806, METHOD_BUFFERED,                             \
           FILE_READ_DATA | FILE_WRITE_DATA)
#define RESTORE_BEOBJECT_CALLBACKS_IOCTL                                       \
  CTL_CODE(FILE_DEVICE_KS, 0x807, METHOD_BUFFERED,                             \
           FILE_READ_DATA | FILE_WRITE_DATA)

// Global variable to our device.
PDEVICE_OBJECT deviceObj = NULL;

// QWORD
typedef unsigned __int64 QWORD;

// OLD_CALLBACKS
typedef struct _OLD_CALLBACKS {
  QWORD PreOperationProc;
  QWORD PostOperationProc;
  QWORD PreOperationThread;
  QWORD PostOperationThread;
} OLD_CALLBACKS, *POLD_CALLBACKS;

// CALLBACK_ENTRY
typedef struct _CALLBACK_ENTRY {
  WORD Version;                    // 0x0
  WORD OperationRegistrationCount; // 0x2
  DWORD unk1;                      // 0x4
  PVOID RegistrationContext;       // 0x8
  UNICODE_STRING Altitude;         // 0x10
} CALLBACK_ENTRY,
    *PCALLBACK_ENTRY; // header size: 0x20 (0x6C if you count the array
                      // afterwards - this is only the header. The array of
                      // CALLBACK_ENTRY_ITEMs is useless.)

// CALLBACK_ENTRY_ITEM
typedef struct _CALLBACK_ENTRY_ITEM {
  LIST_ENTRY CallbackList;                    // 0x0
  OB_OPERATION Operations;                    // 0x10
  DWORD Active;                               // 0x14
  CALLBACK_ENTRY *CallbackEntry;              // 0x18
  PVOID ObjectType;                           // 0x20
  POB_PRE_OPERATION_CALLBACK PreOperation;    // 0x28
  POB_POST_OPERATION_CALLBACK PostOperation;  // 0x30
  QWORD unk1;                                 // 0x38
} CALLBACK_ENTRY_ITEM, *PCALLBACK_ENTRY_ITEM; // size: 0x40

// Dummy object callback functions.
OB_PREOP_CALLBACK_STATUS
DummyObjectPreCallback(PVOID RegistrationContext,
                       POB_PRE_OPERATION_INFORMATION OperationInformation) {
  return (OB_PREOP_SUCCESS);
}
VOID DummyObjectPostCallback(
    PVOID RegistrationContext,
    POB_POST_OPERATION_INFORMATION OperationInformation) {
  return;
}