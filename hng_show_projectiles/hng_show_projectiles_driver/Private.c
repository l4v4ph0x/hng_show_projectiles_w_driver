#include "Private.h"
#include <Ntstrsafe.h>

/// <summary>
/// Get page hardware PTE.
/// Address must be valid, otherwise bug check is imminent
/// </summary>
/// <param name="pAddress">Target address</param>
/// <returns>Found PTE</returns>
PMMPTE GetPTEForVA(IN PVOID pAddress, IN WinVer ver, IN ULONG_PTR DYN_PDE_BASE, IN ULONG_PTR DYN_PTE_BASE) {
  if (ver >= WINVER_10_RS1) {
    // Check if large page
    PMMPTE pPDE = (PMMPTE)(
        ((((ULONG_PTR)pAddress >> PDI_SHIFT) << PTE_SHIFT) & 0x3FFFFFF8ull) +
        DYN_PDE_BASE);
    if (pPDE->u.Hard.LargePage)
      return pPDE;

    return (PMMPTE)(
        ((((ULONG_PTR)pAddress >> PTI_SHIFT) << PTE_SHIFT) & 0x7FFFFFFFF8ull) +
        DYN_PTE_BASE);
  } else {
    // Check if large page
    PMMPTE pPDE = MiGetPdeAddress(pAddress);
    if (pPDE->u.Hard.LargePage)
      return pPDE;

    return MiGetPteAddress(pAddress);
  }
}