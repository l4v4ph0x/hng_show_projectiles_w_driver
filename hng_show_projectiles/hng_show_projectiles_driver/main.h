/*******************************************************************************
 *
 *  (C) COPYRIGHT AUTHORS, 2016 - 2017
 *
 *  TITLE:       MAIN.H
 *
 *  VERSION:     1.01
 *
 *  DATE:        20 Apr 2017
 *
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
 * ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 *******************************************************************************/

#pragma once
#include <ntddk.h>
#include "NativeStructs.h"
#include "Private.h"

// from http://projectgames.club/files/Xbox/1%5D%20XBOX/Xboxkrnl_BuildEnvironment/WINDDK/3790.1830/inc/ddk/w2k/ntstrsafe.h
typedef __nullterminated const wchar_t *NTSTRSAFE_PCWSTR;

NTKERNELAPI
NTSTATUS
IoCreateDriver(IN PUNICODE_STRING DriverName,
               OPTIONAL IN PDRIVER_INITIALIZE InitializationFunction);


NTKERNELAPI
NTSTATUS
PsLookupProcessByProcessId(_In_ HANDLE ProcessId, _Outptr_ PEPROCESS *Process);

NTSTATUS NTAPI MmCopyVirtualMemory(PEPROCESS SourceProcess, PVOID SourceAddress,
                                   PEPROCESS TargetProcess, PVOID TargetAddress,
                                   SIZE_T BufferSize,
                                   KPROCESSOR_MODE PreviousMode,
                                   PSIZE_T ReturnSize);

NTKERNELAPI
PVOID
NTAPI
PsGetProcessWow64Process(IN PEPROCESS Process);


#define ABSOLUTE(wait) (wait)

#define RELATIVE(wait) (-(wait))

#define NANOSECONDS(nanos) (((signed __int64)(nanos)) / 100L)

#define MICROSECONDS(micros) (((signed __int64)(micros)) * NANOSECONDS(1000L))

#define MILLISECONDS(milli) (((signed __int64)(milli)) * MICROSECONDS(1000L))

#define SECONDS(seconds) (((signed __int64)(seconds)) * MILLISECONDS(1000L))

NTKERNELAPI VOID
ProbeForWrite(__in_data_source(USER_MODE) volatile VOID *Address, SIZE_T Length,
              ULONG Alignment);


/// <summary>
/// PDE/PTE dynamic code offsets
/// </summary>
typedef struct _TABLE_OFFSETS {
  int PDE;
  int PTE;
} TABLE_OFFSETS, *PTABLE_OFFSETS;

/// <summary>
/// Pre/Post 'meltdown' patch offsets
/// </summary>
typedef struct _TABLE_OFFSETS_MELT {
  // selector[0] - offsets for builds before 'meltdown' patch
  // selector[1] - offsets for builds after  'meltdown' patch
  TABLE_OFFSETS selector[2];
} TABLE_OFFSETS_MELT, *PTABLE_OFFSETS_MELT;

_Dispatch_type_(IRP_MJ_DEVICE_CONTROL) DRIVER_DISPATCH DevioctlDispatch;
_Dispatch_type_(IRP_MJ_CREATE) DRIVER_DISPATCH CreateDispatch;
_Dispatch_type_(IRP_MJ_CLOSE) DRIVER_DISPATCH CloseDispatch;

_Dispatch_type_(IRP_MJ_CREATE) _Dispatch_type_(
    IRP_MJ_CREATE_NAMED_PIPE) _Dispatch_type_(IRP_MJ_CLOSE)
    _Dispatch_type_(IRP_MJ_READ) _Dispatch_type_(IRP_MJ_WRITE) _Dispatch_type_(
        IRP_MJ_QUERY_INFORMATION) _Dispatch_type_(IRP_MJ_SET_INFORMATION)
        _Dispatch_type_(IRP_MJ_QUERY_EA) _Dispatch_type_(
            IRP_MJ_SET_EA) _Dispatch_type_(IRP_MJ_FLUSH_BUFFERS)
            _Dispatch_type_(IRP_MJ_QUERY_VOLUME_INFORMATION) _Dispatch_type_(
                IRP_MJ_SET_VOLUME_INFORMATION)
                _Dispatch_type_(IRP_MJ_DIRECTORY_CONTROL) _Dispatch_type_(
                    IRP_MJ_FILE_SYSTEM_CONTROL)
                    _Dispatch_type_(IRP_MJ_DEVICE_CONTROL) _Dispatch_type_(
                        IRP_MJ_INTERNAL_DEVICE_CONTROL)
                        _Dispatch_type_(IRP_MJ_SHUTDOWN) _Dispatch_type_(
                            IRP_MJ_LOCK_CONTROL) _Dispatch_type_(IRP_MJ_CLEANUP)
                            _Dispatch_type_(IRP_MJ_CREATE_MAILSLOT)
                                _Dispatch_type_(IRP_MJ_QUERY_SECURITY)
                                    _Dispatch_type_(IRP_MJ_SET_SECURITY)
                                        _Dispatch_type_(IRP_MJ_POWER)
                                            _Dispatch_type_(
                                                IRP_MJ_SYSTEM_CONTROL)
                                                _Dispatch_type_(
                                                    IRP_MJ_DEVICE_CHANGE)
                                                    _Dispatch_type_(
                                                        IRP_MJ_QUERY_QUOTA)
                                                        _Dispatch_type_(
                                                            IRP_MJ_SET_QUOTA)
                                                            _Dispatch_type_(
                                                                IRP_MJ_PNP)
                                                                DRIVER_DISPATCH
    UnsupportedDispatch;

DRIVER_INITIALIZE DriverInitialize;
DRIVER_INITIALIZE DriverEntry;
#pragma alloc_text(INIT, DriverEntry)

#define DUMMYDRV_REQUEST1                                                      \
  CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0969, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

typedef struct _INOUT_PARAM {
  ULONG Req;
  ULONG Param1;
  ULONG Param2;
  ULONG Param3;
  ULONG Param4;
  ULONG Param5;
  CHAR Param6[256];
  ULONG Param7;
  ULONG Param8;
} INOUT_PARAM, *PINOUTPARAM;