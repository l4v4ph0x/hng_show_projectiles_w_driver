#include "../cheader/player.h"

#include "../cheader/offsets.h"
#include "../cheader/csummoner.h"
#include "../cheader/oldDays.h"

#include <cmath>
#include <stdio.h>

extern player *localPlayer;

unsigned long __stdcall thread_update(void *v) {
  player *p = (player *)v;

  for (;; Sleep(1)) {
    p->update();
  }
}

player::player(unsigned long pid, unsigned long base) {
  unsigned long pointed;
  id = base;
  processId = pid;

  //CreateThread(0, 0, thread_update, this, 0, 0);
  update();
}

char arr1[0x256];
char arr2[0x256];
unsigned long null = 0;

void player::update() { 
  unsigned long pointed;
  unsigned long pointed2;

  keReadVirtualMemory(processId, (id + OFFSET_ARR2), (unsigned long)&pointed2, 4);
  keReadVirtualMemory(processId, (id + OFFSET_ARR1), (unsigned long)&pointed, 4);
  keReadVirtualMemory(processId, pointed2, (unsigned long)arr2, 0x255);
  keReadVirtualMemory(processId, pointed, (unsigned long)arr1, 0x255);

  x = *(float *)(arr2 + OFFSET_X2);
  y = *(float *)(arr2 + OFFSET_X2 +4);
  z = *(float *)(arr2 + OFFSET_X2 +8);
  look_x = *(float *)(arr1 + OFFSET_LOOK_X);
  look_y = *(float *)(arr1 + OFFSET_LOOK_X +4);
  hp = *(short *)(arr1 + OFFSET_HP);
  side = *(unsigned char *)(arr1 + OFFSET_SIDE);
  status = *(unsigned long *)(arr1 + OFFSET_STATUS);
  //visible = *(bool *)(arr2 + OFFSET_VISIBILITY);


  if (hp > 0 && hp < 150 && memcmp(&x, &null, 4) != 0) {
    dead = false;

    look_x = hngAimToDegree(getOldDaysAim(look_x));
    if (look_x < 0) {
      look_x = 360 - abs(look_x);
    }

    look_y = hngAimToDegreeY(look_y);

    if (localPlayer && !localPlayer->dead) {
      double adjance = std::abs(localPlayer->z - z);
      double opposite = std::abs(localPlayer->x - x);

      if (x < localPlayer->x && z < localPlayer->z) {
        degreeFromLocalPlayer = 360 - degrees(atan2(opposite, adjance));
      } else if (x < localPlayer->x && z > localPlayer->z) {
        degreeFromLocalPlayer = 270 - degrees(atan2(adjance, opposite));
      } else if (x > localPlayer->x && z > localPlayer->z) {
        degreeFromLocalPlayer = 90 + degrees(atan2(adjance, opposite));
      } else if (x > localPlayer->x && z < localPlayer->z) {
        // local player degree < 90 && local player degree > 270
        degreeFromLocalPlayer = degrees(atan2(opposite, adjance));
      }

      float localPlayerDegreeX = localPlayer->look_x - 180;
      if (localPlayerDegreeX < 0) {
        localPlayerDegreeX += 360;
      } else if (localPlayerDegreeX > 360) {
        localPlayerDegreeX -= 360;
      }

      // printf("%f\n", localPlayerDegreeX);

      degreeFromLocalPlayer -= localPlayerDegreeX;
      if (degreeFromLocalPlayer < 0) {
        degreeFromLocalPlayer += 360;
      }

      hyp = std::sqrt((adjance * adjance) + (opposite * opposite));

    }
  } else {
    dead = true;
  }
}