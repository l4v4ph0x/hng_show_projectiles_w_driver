#include "../cheader/csummoner.h"
#include <TlHelp32.h>

#include <psapi.h>
#include <tchar.h>

#pragma comment(lib, "psapi.lib")

#define NT_SUCCESS(x) ((x) >= 0)
#define size_to_scan 786

#define NT_SUCCESS(x) ((x) >= 0)
#define STATUS_INFO_LENGTH_MISMATCH 0xc0000004
#define SystemProcessAndThreadInformation 5

typedef LONG KPRIORITY;

typedef struct _CLIENT_ID {
  DWORD UniqueProcess;
  DWORD UniqueThread;
} CLIENT_ID;

typedef struct _SYSTEM_THREADS {
  LARGE_INTEGER KernelTime;
  LARGE_INTEGER UserTime;
  LARGE_INTEGER CreateTime;
  ULONG WaitTime;
  PVOID StartAddress;
  CLIENT_ID ClientId;
  KPRIORITY Priority;
  KPRIORITY BasePriority;
  ULONG ContextSwitchCount;
  LONG State;
  LONG WaitReason;
} SYSTEM_THREADS, *PSYSTEM_THREADS;

typedef NTSTATUS(WINAPI *tNTQSI)(ULONG SystemInformationClass,
                                 PVOID SystemInformation,
                                 ULONG SystemInformationLength,
                                 PULONG ReturnLength);

typedef struct _UNICODE_STRING {
  USHORT Length;
  USHORT MaximumLength;
  PWSTR Buffer;
} UNICODE_STRING;

typedef struct _VM_COUNTERS {
#ifdef _WIN64
  SIZE_T PeakVirtualSize;
  SIZE_T PageFaultCount;
  SIZE_T PeakWorkingSetSize;
  SIZE_T WorkingSetSize;
  SIZE_T QuotaPeakPagedPoolUsage;
  SIZE_T QuotaPagedPoolUsage;
  SIZE_T QuotaPeakNonPagedPoolUsage;
  SIZE_T QuotaNonPagedPoolUsage;
  SIZE_T PagefileUsage;
  SIZE_T PeakPagefileUsage;
  SIZE_T VirtualSize;
#else
  SIZE_T PeakVirtualSize;
  SIZE_T VirtualSize;
  ULONG PageFaultCount;
  SIZE_T PeakWorkingSetSize;
  SIZE_T WorkingSetSize;
  SIZE_T QuotaPeakPagedPoolUsage;
  SIZE_T QuotaPagedPoolUsage;
  SIZE_T QuotaPeakNonPagedPoolUsage;
  SIZE_T QuotaNonPagedPoolUsage;
  SIZE_T PagefileUsage;
  SIZE_T PeakPagefileUsage;
#endif
} VM_COUNTERS;

typedef struct _SYSTEM_PROCESSES {
  ULONG NextEntryDelta;
  ULONG ThreadCount;
  ULONG Reserved1[6];
  LARGE_INTEGER CreateTime;
  LARGE_INTEGER UserTime;
  LARGE_INTEGER KernelTime;
  UNICODE_STRING ProcessName;
  KPRIORITY BasePriority;
  ULONG ProcessId;
  ULONG InheritedFromProcessId;
  ULONG HandleCount;
  ULONG Reserved2[2];
  VM_COUNTERS VmCounters;
#if _WIN32_WINNT >= 0x500
  IO_COUNTERS IoCounters;
#endif
  SYSTEM_THREADS Threads[1];
} SYSTEM_PROCESSES, *PSYSTEM_PROCESSES;


unsigned long get_module(unsigned long pid, const char *module_name) {
  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
  MODULEENTRY32 me32;
  me32.dwSize = sizeof(MODULEENTRY32);

  while (Module32Next(snapshot, &me32)) {
    if (strcmp(me32.szModule, module_name) == 0) {
      return (unsigned long)me32.modBaseAddr;
    }
  }

  return 0;
}

unsigned long get_proc_light(const char *name) {
  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32))
    if (memcmp(pe32.szExeFile, name, sizeof(name)) == 0)
      return pe32.th32ProcessID;

  return 0;
}

unsigned long get_proc(const wchar_t *name) {
  ULONG cbBuffer = 131072;
  PVOID pBuffer = NULL;
  NTSTATUS Status = STATUS_INFO_LENGTH_MISMATCH;
  HANDLE hHeap = GetProcessHeap();
  tNTQSI fpQSI = (tNTQSI)GetProcAddress(GetModuleHandle(_T("ntdll.dll")),
                                        "NtQuerySystemInformation");

  while (1) {
    pBuffer = HeapAlloc(hHeap, HEAP_ZERO_MEMORY, cbBuffer);
    if (pBuffer == NULL)
      return 0;

    Status =
        fpQSI(SystemProcessAndThreadInformation, pBuffer, cbBuffer, &cbBuffer);

    if (Status == STATUS_INFO_LENGTH_MISMATCH) {
      HeapFree(hHeap, NULL, pBuffer);
      cbBuffer *= 2;
    } else if (!NT_SUCCESS(Status)) {
      HeapFree(hHeap, NULL, pBuffer);
      return 0;
    } else {
      PSYSTEM_PROCESSES infoP = NULL;
      infoP = (PSYSTEM_PROCESSES)pBuffer;

      while (infoP) {
        if (infoP->ProcessName.Buffer != 0 &&
            wcscmp(infoP->ProcessName.Buffer, name) == 0) {
          return (unsigned long)infoP->ProcessId;
        }

        if (!infoP->NextEntryDelta)
          break;
        infoP = (PSYSTEM_PROCESSES)(((LPBYTE)infoP) + infoP->NextEntryDelta);
      }
      if (pBuffer)
        HeapFree(GetProcessHeap(), NULL, pBuffer);
    }
  }
}
