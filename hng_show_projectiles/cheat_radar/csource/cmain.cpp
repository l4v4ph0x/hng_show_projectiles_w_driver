#include <algorithm>
#include <assert.h>
#include <codecvt>
#include <comdef.h>
#include <locale>
#include <stdio.h>
#include <string>
#include <vector>
#include <windows.h>

#include "../cheader/csummoner.h"
#include "../cheader/exwin.h"
#include "../cheader/offsets.h"
#include "../cheader/player.h"

// disable funking _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

void writeFile(char *arr);

char oldJumptoFunPlayers[6];
char oldJumptoFunLocal[7];

unsigned char jumpToDetourFunPlayers[6] = "\xE9\x38\xBE\x34\x00";
unsigned char detourfunPlayers[21] = "\x89\x3D\x90\x90\x90\x90\x66\x83\x78\x7F"
                                     "\x00\xE9\xBA\x41\xCB\xFF\x00\x00\x00\x00";

unsigned char jumpToDetourFunLocal[7] = "\xE9\x22\xA9\x34\x00\x90";
unsigned char detourfunLocal[19] = "\x89\x3D\x90\x90\x90\x90\x8B\x87\x68"
                                   "\x48\x00\x00\xE9\xCE\x56\xCB\xFF";

std::vector<player *> players;
player *localPlayer;
char hngLocation[256];

bool *hngDead = new bool(false);
bool oldBack = false;

player *getPlayer(unsigned long pid, unsigned long dataAddr) {
  unsigned long data;

  data = 0;

  keReadVirtualMemory(pid, dataAddr, (unsigned long)&data, 4);
  if (data == 0) {
    return nullptr;
  }

  player *p = new player(pid, data);
  if (p->x == 0.0 || p->y == 0.0 || p->z == 0.0 || p->hp == 0) {
    delete p;
    return nullptr;
  } else {
    /*
    unsigned long pointed;
    unsigned char datao[300];

    keReadVirtualMemory(pid, (data + OFFSET_ARR1), (unsigned long)&pointed, 4);
    keReadVirtualMemory(pid, pointed, (unsigned long)&datao, 300);

    for (int i = 0; i < 300 / 2; i++) {
      printf("%02x: %d\n", i, *(unsigned short *)(datao + i));
    }
    */
    /*
    unsigned long pointed;
    unsigned char datao[300];
    unsigned char stringo[256];

    keReadVirtualMemory(pid, (data + OFFSET_ARR2), (unsigned long)&pointed, 4);
    //keReadVirtualMemory(pid, pointed, (unsigned long)&datao, 300);

    for (int i = 0; i < 300; i++) {
      keReadVirtualMemory(pid, (pointed + i), (unsigned long)&stringo,
                          256);

      //if (stricmp((const char *)stringo, "testte1") == 0) {
        printf("%02x: %s\n", i, stringo);
      //}
    }
    */
    return p;
  }
}

void __stdcall closeHandler(unsigned long cevent) {
  unsigned long pid;
  unsigned long playerDll;

  if (!oldBack) {
    playerDll = 0;

    if (!*hngDead) {
      pid = get_proc_light("hng.exe");

      if (pid == 0) {
        char buf[1024];
        sprintf(
            buf,
            "cd %s\"\nhng.exe /M 3484085725860281215 /P 8442615574753884439 /W "
            "37.48.111.86:15130 /lang en /NOSERVER ",
            hngLocation);
        writeFile(buf);
        WinExec("run.bat", 0);
        pid = get_proc(L"hng.exe");
        system("del run.bat");

        for (; playerDll == 0; Sleep(10)) {
          playerDll = get_module(pid, "player.dll");
        }
      } else {
        keGetProcessModule(pid, (unsigned long)&playerDll, L"player.dll");
      }

      if (pid && playerDll) {
        // jump to deoure back to old ones
        keWriteProtectedMemory(pid, playerDll + OFFSET_PLAYERS,
                               (unsigned long)oldJumptoFunPlayers, 5,
                               PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

        keWriteProtectedMemory(pid, playerDll + OFFSET_LOCAL_PLAYER,
                               (unsigned long)oldJumptoFunLocal, 6,
                               PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
      }
    }
  }
}

char *readFile(const char *filename) {
  FILE *f = fopen(filename, "rt");
  assert(f);
  fseek(f, 0, SEEK_END);
  long length = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buffer = (char *)malloc(length + 1);
  buffer[length] = '\0';
  fread(buffer, 1, length, f);
  fclose(f);
  return buffer;
}

void writeFile(char *arr) {
  FILE *fp;

  fp = fopen("run.bat", "w");
  fprintf(fp, arr);

  fclose(fp);
}

char *getFileDir(char *cmdline) {
  char ret[1024];
  memcpy(ret, cmdline, 1024);

  for (int i = strlen(ret) - 1; i >= 0; i--) {
    if (ret[i] == '\\') {
      ret[i + 1] = 0;
      break;
    }
  }

  return ret;
}

std::string wstringToUtf8(const std::wstring &str) {
  std::wstring_convert<std::codecvt_utf8<wchar_t>> strCnv;
  return strCnv.to_bytes(str);
}

unsigned long __stdcall thread_checkHng(void *p) {
  unsigned long pid;
  unsigned long playerDll;

  pid = 1;
  playerDll = 0;

  for (; pid != 0; Sleep(100)) {
    pid = get_proc_light("hng.exe");
  }
  *hngDead = true;

  return 0;
}

unsigned long __stdcall thread_aimbot(void *p) {
  HWND hw_hng;
  unsigned long null = 0;
  float precX = 0.3f;
  double normalYH = 0.1030081081081081;
  double normalYL = 0.1129081081081081;

  hw_hng = FindWindowA(0, "H&G");

  for (;; Sleep(10)) {
    int closestRange = -1;
    unsigned long selectedID = -1;

    for (int i = 0; i < players.size(); i++) {
      // check for living player
      if (players[i]->hp > 0 && players[i]->hp < 150 &&
          players[i]->id != localPlayer->id &&
          memcmp(&players[i]->x, &null, 4) != 0) {
        // check for only enemy players
        if (players[i]->side != localPlayer->side) {
          // if we havent got already closest then set first
          if (selectedID == -1) {
            closestRange = players[i]->hyp;
            selectedID = i;
          } else {
            // else compare whos closest
            if (players[i]->hyp < closestRange) {
              closestRange = players[i]->hyp;
              selectedID = i;
            }
          }
        }
      }
    }

    if ((GetAsyncKeyState(0x43) & 0x8000) && selectedID != -1) {
      // find closest target
      double degreeToAimY;
      double faraway;
      double adjanceY = std::abs(localPlayer->y - players[selectedID]->y);
      
      //printf("closest: %08X %f\n", players[selectedID], players[selectedID]->degreeFromLocalPlayer);

      if (players[selectedID]->y < localPlayer->y) {
        faraway = players[selectedID]->hyp - players[selectedID]->hyp * normalYL;
        degreeToAimY = 180 - atan2(faraway, adjanceY) / PI * 180.0;

        faraway -= faraway * (normalYL - degreeToAimY / 9400);
        degreeToAimY = 180 - atan2(faraway, adjanceY) / PI * 180.0;
      } else {
        faraway = players[selectedID]->hyp - players[selectedID]->hyp * normalYH;
        degreeToAimY = atan2(faraway, adjanceY) / PI * 180.0;

        faraway -= faraway * (normalYH + degreeToAimY / 7400);
        degreeToAimY = atan2(faraway, adjanceY) / PI * 180.0;
      }

      //if (players[selectedID]->status == STATUS_CROUCH) {
      //  degreeToAimY += ((0.236 / faraway) * 100) / 2;
      //} else if (players[selectedID]->status == STATUS_LIE) {
      //  degreeToAimY += ((0.430 / faraway) * 100);
      //} else {
        degreeToAimY += ((0.130 / faraway) * 100);
      //}
      
      if (players[selectedID]->degreeFromLocalPlayer > 180.f) {
        mouse_event(MOUSEEVENTF_MOVE,
                    (players[selectedID]->degreeFromLocalPlayer - 360.f),
                    (degreeToAimY - localPlayer->look_y),
                    NULL, NULL);
      } else {
        mouse_event(
          MOUSEEVENTF_MOVE,
          players[selectedID]->degreeFromLocalPlayer,
          (degreeToAimY - localPlayer->look_y),
          NULL, NULL);
      }
    }
  }
}

void main() {
  unsigned long terminal;
  unsigned long pid;
  unsigned long bepid;
  unsigned long playerDll;
  unsigned long globalsDll;
  unsigned long dataAddrPlayers;
  unsigned long dataAddrLocal;
  char getcmdlinebuf[256];
  unsigned long null;

  playerDll = 0;
  globalsDll = 0;
  localPlayer = nullptr;
  null = 0;

  printf("||=======================================================||\n");
  printf("||          cheat for hng version 150912 QQ              ||\n");
  printf("||                     radar                             ||\n");
  printf("|| made by: lava                                         ||\n");
  printf("||=======================================================||\n");

  printf("waiting for hng\n");
  pid = get_proc(L"hng.exe");
  bepid = get_proc(L"hng_BE.exe");

  printf("searching modules ... ");
  for (; playerDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&playerDll, L"player.dll");
  }
  for (; globalsDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&globalsDll, L"globals.dll");
  }

  sprintf(getcmdlinebuf, "wmic process where handle='%d' get commandLine > res",
          bepid);
  system(getcmdlinebuf);
  char *out = readFile("res");
  system("del res");

  wchar_t buf[1024];
  swprintf(buf, sizeof(buf) / sizeof(*buf), L"%ls", out + 386 * 2);

  _bstr_t b(buf);
  char *c = b;

  char *path = getFileDir(c);
  // memcpy(hngLocation, path +2, strlen(path) + 1);
  std::string s(path);
  std::replace(s.begin(), s.end(), '\n', ' ');
  std::replace(s.begin(), s.end(), '\r', ' ');
  memcpy(hngLocation, s.c_str(), strlen(s.c_str()) + 1);

  //ShowWindow(GetConsoleWindow(), 0);

  if (playerDll != 0 && globalsDll != 0) {
    *hngDead = false;
    printf("| Done\n");

    keReadVirtualMemory(pid, globalsDll + OFFSET_TERMINAL,
                        (unsigned long)&terminal, 4);

    dataAddrPlayers = terminal + OFFSET_ADDR_PLAYERS;
    dataAddrLocal = terminal + OFFSET_ADDR_LOCAL;

    // write dynamic addrs
    *(unsigned long *)(detourfunPlayers + 2) = dataAddrPlayers;
    *(unsigned long *)(detourfunPlayers + 12) =
        OFFSET_PLAYERS - OFFSET_DETOUR_FUNC_PLAYERS - 0xB;

    *(unsigned long *)(detourfunLocal + 2) = dataAddrLocal;
    *(unsigned long *)(detourfunLocal + 13) =
        OFFSET_LOCAL_PLAYER - OFFSET_DETOUR_FUNC_LOCAL - 0xB;

    *(unsigned long *)(jumpToDetourFunPlayers + 1) =
        OFFSET_DETOUR_FUNC_PLAYERS - OFFSET_PLAYERS - 5;
    *(unsigned long *)(jumpToDetourFunLocal + 1) =
        OFFSET_DETOUR_FUNC_LOCAL - OFFSET_LOCAL_PLAYER - 5;

    // set close handler
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)closeHandler, TRUE);

    // thread that check if hng is still running
    CreateThread(0, 0, thread_checkHng, 0, 0, 0);

    // read old bytes to change back when exit
    keReadVirtualMemory(pid, playerDll + OFFSET_PLAYERS,
                        (unsigned long)&oldJumptoFunPlayers, 5);
    keReadVirtualMemory(pid, playerDll + OFFSET_LOCAL_PLAYER,
                        (unsigned long)&oldJumptoFunLocal, 6);

    // create detour
    keWriteProtectedMemory(pid, playerDll + OFFSET_DETOUR_FUNC_PLAYERS,
                           (unsigned long)detourfunPlayers, 20,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    keWriteProtectedMemory(pid, playerDll + OFFSET_DETOUR_FUNC_LOCAL,
                           (unsigned long)detourfunLocal, 18,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    // jump to deoure
    keWriteProtectedMemory(pid, playerDll + OFFSET_PLAYERS,
                           (unsigned long)jumpToDetourFunPlayers, 5,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    keWriteProtectedMemory(pid, playerDll + OFFSET_LOCAL_PLAYER,
                           (unsigned long)jumpToDetourFunLocal, 6,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    loadExWin();

    printf("gettin players\n");
    // player *p = getPlayer(pid, dataAddrLocal);
    // system("pause");
    // exit(0);

    // create thread that helpts to aim
    CreateThread(0, 0, thread_aimbot, 0, 0, 0);

    for (; !*hngDead; Sleep(10)) {
      unsigned long pointed = 0;
      // check that we always have local player
      player *p = getPlayer(pid, dataAddrLocal);

      if (p && p->hp > 0 && p->hp < 150 && memcmp(&p->x, &null, 4) != 0) {
        if (localPlayer) {
          delete localPlayer;
        }

        localPlayer = p;

        if (localPlayer) {
          unsigned int size = players.size();

          for (int i = 0; i < players.size(); i++) {
            players[i]->update();

            if (players[i]->hp > 0 || players[i]->hp < 150 ) {
            } else {
              try {
              } catch (...) {
                printf("remove player: %08X x:%f y:%f z:%f hp:%d\n", p->id,
                       p->x, p->y, p->z, p->hp);

                players.erase(players.begin() + i - 1);
                i--;
              }
              
            }
          }

          player *p = getPlayer(pid, dataAddrPlayers);

          // always make sure we got it
          if (p) {
            // check if this is in array already
            for (int i = 0; i < size; i++) {
              if (players[i]->id == p->id) {
                // if we have it already then delete
                try {
                  delete p;
                  p = nullptr;
                } catch (...) {
                }

                break;
              }
            }

            // if we havent deleted p then we didnt had it
            if (p) {
              players.push_back(p);
              printf("got new player: %08X x:%f y:%f z:%f hp:%d %f %f\n", p->id,
                     p->x, p->y, p->z, p->hp, p->degreeFromLocalPlayer, p->hyp);
            }
          }
        }
      } else {
        if (localPlayer) {
          localPlayer->dead = true;
        }
      }
    }

    printf("looks like hng is dead\n");

    // null vars
    pid = 0;
    playerDll = 0;

    printf("let me fix player dll for next run\n");

    char buf[1024];
    sprintf(buf,
            "cd %s\"\nhng.exe /M 3484085725860281215 /P 8442615574753884439 /W "
            "37.48.111.86:15130 /lang en /NOSERVER ",
            hngLocation);
    writeFile(buf);

    WinExec("run.bat", 0);
    pid = get_proc(L"hng.exe");
    system("del run.bat");

    for (; playerDll == 0; Sleep(10)) {
      playerDll = get_module(pid, "player.dll");
    }

    // jump to deoure back to old ones
    keWriteProtectedMemory(pid, playerDll + OFFSET_PLAYERS,
                           (unsigned long)oldJumptoFunPlayers, 5,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    keWriteProtectedMemory(pid, playerDll + OFFSET_LOCAL_PLAYER,
                           (unsigned long)oldJumptoFunLocal, 6,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    // make it true so closehandler dont have to write old bytes back also
    oldBack = true;

    printf("done u can quit me now\n");
  } else {
    printf("| Failed\n");
  }
}
