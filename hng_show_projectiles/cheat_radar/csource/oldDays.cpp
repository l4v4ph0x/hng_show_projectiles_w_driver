#include "../cheader/oldDays.h"

#define maxHngAimX 6.283081667f
#define maxHngAimY 1.39626348f

float getOldDaysAim(float newAim) {
  if (newAim < 0) {
    int timestomulti = (newAim / -maxHngAimX) + 1;
    newAim += timestomulti * maxHngAimX;
  } else {
    int timestomulti = (newAim / maxHngAimX) + 1;
    newAim -= timestomulti * maxHngAimX;
  }

  return maxHngAimX - newAim;
}

double hngAimToDegree(float aim) {
  return 360 / maxHngAimX * aim;
}

double hngAimToDegreeY(float aim) {
  return 180 / (maxHngAimY * 2) * aim + 90;
}

float degreeToHngAim(float aim) {
  return maxHngAimX / 360 * aim;
}

float degreeToHngAimY(float aim) {
  return (maxHngAimY * 2) / 180 * aim - maxHngAimY;
}
