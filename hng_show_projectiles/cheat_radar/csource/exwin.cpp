#include "../cheader/exwin.h"
#include "../cheader/offsets.h"
#include "../cheader/player.h"

#include <vector>
#include <windows.h>
#include <assert.h>
#include <gdiplus.h>


#pragma comment(lib, "gdiplus.lib")

// disable funking _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

int exwinWidth = 320;
int exwinHeight = 300;

extern std::vector<player *> players;
extern player *localPlayer;
extern bool *hngDead;

HWND thisWindow;

bool showFriendly = true;
bool showEnemy = true;

bool dotsizes[3] = {true, false, false};
int dotSize = 2;

void SetWndTaskbarTab(HWND hWnd, BOOL bShowOnTaskbar) {
  LONG lStyleEx = GetWindowLong(hWnd, GWL_EXSTYLE);

  lStyleEx &= !(bShowOnTaskbar ? WS_EX_TOOLWINDOW : WS_EX_APPWINDOW);
  lStyleEx |= (bShowOnTaskbar ? WS_EX_APPWINDOW : WS_EX_TOOLWINDOW);

  ShowWindow(hWnd, SW_HIDE);
  SetWindowLong(hWnd, GWL_EXSTYLE, lStyleEx);
  ShowWindow(hWnd, SW_SHOWNOACTIVATE);
}

void drawPoint(HDC dc, int x, int y, int w, int h, COLORREF color) {
  for (int i = 0; i < w; i++) {
    for (int j = 0; j < h; j++) {
      SetPixel(dc, x + i, y + j, color);
    }
  }
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam,
                         LPARAM lparam) {
  switch (message) {
  case WM_CHAR: // this is just for a program exit besides window's
                // borders/taskbar
    if (wparam == VK_ESCAPE) {
      DestroyWindow(hwnd);
    }
  case WM_PAINT: {
    thisWindow = hwnd;

    break;
  }
  case WM_SIZE: {
    RECT rct;
    GetClientRect(hwnd, &rct);

    exwinWidth = rct.right - rct.left;
    exwinHeight = rct.bottom - rct.top;

    break;
  }
  case WM_RBUTTONDOWN: {
    HMENU hPopupMenu = CreatePopupMenu();
    char buf[256];
    char buf2[256];
    char buf3[256];
    char buf4[256];
    char buf5[256];

    sprintf(buf, "%s toggle friendly players",
            (showFriendly == true ? "+" : "-"));
    sprintf(buf2, "%s toggle enemy players", (showEnemy == true ? "+" : "-"));

    sprintf(buf3, "%s dot size 1", (dotsizes[0] == true ? "+" : "-"));
    sprintf(buf4, "%s dot size 2", (dotsizes[1] == true ? "+" : "-"));
    sprintf(buf5, "%s dot size 3", (dotsizes[2] == true ? "+" : "-"));

    InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING, ID_SHOW_FRIENDLY, buf);
    InsertMenu(hPopupMenu, 1, MF_BYPOSITION | MF_STRING, ID_SHOW_ENEMY, buf2);
    InsertMenu(hPopupMenu, 2, MF_BYPOSITION | MF_STRING, ID_DOT_SIZE1, buf3);
    InsertMenu(hPopupMenu, 3, MF_BYPOSITION | MF_STRING, ID_DOT_SIZE2, buf4);
    InsertMenu(hPopupMenu, 4, MF_BYPOSITION | MF_STRING, ID_DOT_SIZE3, buf5);

    SetForegroundWindow(hwnd);

    POINT p;
    if (GetCursorPos(&p)) {
      TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, p.x, p.y, 0,
                     hwnd, NULL);
    }

    break;
  }
  case WM_COMMAND: {
    if (LOWORD(wparam) == ID_SHOW_FRIENDLY) {
      showFriendly = !showFriendly;
    } else if (LOWORD(wparam) == ID_SHOW_ENEMY) {
      showEnemy = !showEnemy;
    } else if (LOWORD(wparam) == ID_DOT_SIZE1) {
      dotSize = 2;
      dotsizes[0] = true;
      dotsizes[1] = false;
      dotsizes[2] = false;
    } else if (LOWORD(wparam) == ID_DOT_SIZE2) {
      dotSize = 4;
      dotsizes[0] = false;
      dotsizes[1] = true;
      dotsizes[2] = false;
    } else if (LOWORD(wparam) == ID_DOT_SIZE3) {
      dotSize = 6;
      dotsizes[0] = false;
      dotsizes[1] = false;
      dotsizes[2] = true;
    }
    break;
  }
  case WM_DESTROY:
    PostQuitMessage(0);
    exit(0);
    break;
  default:
    return DefWindowProc(hwnd, message, wparam, lparam);
  }
}

unsigned long __stdcall thread_paint(void *p) {
  bool alreadyDead = false;
  unsigned long null = 0;

  Gdiplus::Font font(&Gdiplus::FontFamily(L"Arial"), 12);
  Gdiplus::SolidBrush brush(Gdiplus::Color::Red);

  LPSTREAM pStream;
  HRSRC hRsrc = FindResource(NULL, "PIC1", RT_RCDATA);
  HGLOBAL hGlob1 = LoadResource(NULL, hRsrc);
  int size = SizeofResource(NULL, hRsrc);
  auto hGlobal = GlobalAlloc(GMEM_FIXED, size);
  LPVOID resPtr = LockResource(hGlob1);
  memcpy(hGlobal, resPtr, size);
  FreeResource(hGlob1);
  // create stream
  CreateStreamOnHGlobal(hGlobal, true, &pStream);
  auto ourImage = new Gdiplus::Image(pStream, false);

  for (;; localPlayer ? Sleep(10) : Sleep(500)) {
    if (*hngDead) {
      ShowWindow(thisWindow, 0);
      break;
    }

    if (thisWindow) {
      PAINTSTRUCT ps;
      // HDC dc = BeginPaint(thisWindow, &ps);
      HDC dc = GetDC(thisWindow);

      if (localPlayer && !localPlayer->dead) {
        alreadyDead = false;

        SetBkMode(dc, TRANSPARENT);
        Rectangle(dc, 0, 0, exwinWidth, exwinHeight);

        // draw lines to look
        HPEN hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
        SelectObject(dc, hPen);
        MoveToEx(dc, 0, 0, NULL);
        LineTo(dc, exwinWidth / 2, exwinHeight / 2);
        LineTo(dc, exwinWidth, 0);
        MoveToEx(dc, exwinWidth / 2, 0, NULL);
        LineTo(dc, exwinWidth / 2, exwinHeight / 2);
        DeleteObject(SelectObject(dc, GetStockObject(BLACK_PEN)));

        // draw local player
        // drawPoint(dc, exwinWidth / 2 - dotSize / 2,
        //          exwinHeight / 2 - dotSize / 2, dotSize, dotSize,
        //          RGB(0, 255, 0));

        unsigned int size = players.size();

        for (unsigned int i = 0; i < size; i++) {
          if (players[i]->hp > 0 && players[i]->hp < 150 &&
              players[i]->id != localPlayer->id &&
              memcmp(&players[i]->x, &null, 4) != 0) {
            double adjance = 0, opposite = 0;

            if (players[i]->degreeFromLocalPlayer >= 270) {
              double cosed =
                  cos(radians(players[i]->degreeFromLocalPlayer - 270));
              double sined =
                  sin(radians(players[i]->degreeFromLocalPlayer - 270));

              adjance = cosed * players[i]->hyp;
              opposite = sined * players[i]->hyp;

              adjance *= -1;
            } else if (players[i]->degreeFromLocalPlayer >= 180) {
              double cosed =
                  cos(radians(players[i]->degreeFromLocalPlayer - 180));
              double sined =
                  sin(radians(players[i]->degreeFromLocalPlayer - 180));

              adjance = sined * players[i]->hyp;
              opposite = cosed * players[i]->hyp;

              adjance *= -1;
              opposite *= -1;
            } else if (players[i]->degreeFromLocalPlayer >= 90) {
              double cosed =
                  cos(radians(players[i]->degreeFromLocalPlayer - 90));
              double sined =
                  sin(radians(players[i]->degreeFromLocalPlayer - 90));

              adjance = cosed * players[i]->hyp;
              opposite = sined * players[i]->hyp;

              opposite *= -1;
            } else if (players[i]->degreeFromLocalPlayer >= 0) {
              double cosed = cos(radians(players[i]->degreeFromLocalPlayer));
              double sined = sin(radians(players[i]->degreeFromLocalPlayer));

              adjance = sined * players[i]->hyp;
              opposite = cosed * players[i]->hyp;
              // adjance = cosed * players[i]->hyp;
              // opposite = sin(sined) * players[i]->hyp;
            }

            // int x = (players[i]->x - localPlayer->x) / 2;
            // int z = (players[i]->z - localPlayer->z) / 2;

            int x = exwinWidth / 2 + adjance;
            int z = exwinHeight / 2 - opposite;

            // printf("%f %f\n", adjance, opposite);

            if (players[i]->side == localPlayer->side) {
              if (showFriendly) {
                // draw friendly players
                drawPoint(dc, x - dotSize / 2, z - dotSize / 2, dotSize,
                          dotSize, RGB(0, 0, 255));
              }
            } else {
              if (showEnemy) {
                // if (players[i]->visible) {
                //  // draw lines whos visible
                //  HPEN hPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 255));
                //  SelectObject(dc, hPen);
                //  MoveToEx(dc, x - dotSize / 2, z - dotSize / 2, NULL);
                //  LineTo(dc, exwinWidth / 2, exwinHeight / 2);
                //  DeleteObject(SelectObject(dc, GetStockObject(BLACK_PEN)));
                //}

                drawPoint(dc, x - dotSize / 2, z - dotSize / 2, dotSize,
                          dotSize, RGB(255, 0, 0));
              }
            }
          }
        }
      } else {
        if (!alreadyDead) {
          alreadyDead = true;
          Gdiplus::Graphics gr(dc);
          gr.DrawImage(ourImage, 0, 0, exwinWidth, exwinHeight);
          gr.DrawString(L"Spawn Player!", -1, &font, Gdiplus::PointF(10, 10),
                        &brush);
        }
      }

      // EndPaint(thisWindow, &ps);
      ReleaseDC(thisWindow, dc);
    }
  }

  return 0;
}

unsigned long __stdcall thread_loadExWin(void *p) {
  RECT rect;
  GetWindowRect(GetConsoleWindow(), &rect);

  // Register the window class.
  const char CLASS_NAME[] = "EasyRadar";

  WNDCLASS wc = {};

  wc.lpfnWndProc = WndProc;
  wc.hInstance = 0;
  wc.lpszClassName = CLASS_NAME;

  RegisterClass(&wc);

  Gdiplus::GdiplusStartupInput gdiplusStartupInput;
  ULONG_PTR gdiplusToken;

  Gdiplus::Status st =
      GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

  // Create the window.
  HWND windowHandle =
      CreateWindowEx(0, CLASS_NAME, "easy hng radar by lava",
                     WS_OVERLAPPEDWINDOW | WS_EX_TOOLWINDOW, rect.left,
                     rect.top, exwinWidth, exwinHeight, NULL, NULL, 0, NULL);

  ShowWindow(windowHandle, SW_SHOW);
  SetWndTaskbarTab(windowHandle, false);

  CreateThread(0, 0, thread_paint, 0, 0, 0);

  MSG messages;
  while (GetMessage(&messages, NULL, 0, 0) > 0) {
    TranslateMessage(&messages);
    DispatchMessage(&messages);
  }
  DeleteObject(windowHandle);

  return 0;
}

void loadExWin() { CreateThread(0, 0, thread_loadExWin, 0, 0, 0); }