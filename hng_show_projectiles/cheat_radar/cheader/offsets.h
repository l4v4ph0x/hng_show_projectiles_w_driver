#ifndef OFFSETS_H
#define OFFSETS_H

#define PI (atan(1) * 4)
#define radians(deg) ((deg) * PI / 180.0)
#define degrees(rad) ((rad) * 180.0 / PI)


const unsigned long OFFSET_ARR1 = 0x4868;
const unsigned long OFFSET_ARR2 = 0x4974;

const unsigned long OFFSET_X = 0xC;
const unsigned long OFFSET_LOOK_X = 0x4;
const unsigned long OFFSET_HP = 0x7F;

const unsigned long OFFSET_X2 = 0x30;

const unsigned long OFFSET_SIDE = 0x87;
const unsigned long OFFSET_VISIBILITY = 0x60 -4;
const unsigned long OFFSET_STATUS = 0x156 +4;

const unsigned long OFFSET_TERMINAL = 0x5C520;
const unsigned long OFFSET_ADDR_PLAYERS = 0x4;
const unsigned long OFFSET_ADDR_LOCAL = OFFSET_ADDR_PLAYERS + 4;

const unsigned long OFFSET_PLAYERS = 0x1BBFCC;
const unsigned long OFFSET_LOCAL_PLAYER = OFFSET_PLAYERS + 0x1527;
const unsigned long OFFSET_DETOUR_FUNC_PLAYERS = 0x501B32;
const unsigned long OFFSET_DETOUR_FUNC_LOCAL = OFFSET_DETOUR_FUNC_PLAYERS + 0x24;

#define STATUS_STAND 32
#define STATUS_CROUCH 160
#define STATUS_LIE 100

#endif