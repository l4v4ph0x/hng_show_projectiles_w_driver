#ifndef OLDDAYS_H
#define OLDDAYS_H

float getOldDaysAim(float newAim);
double hngAimToDegree(float aim);
double hngAimToDegreeY(float aim);
float degreeToHngAim(float aim);
float degreeToHngAimY(float aim);

#endif
