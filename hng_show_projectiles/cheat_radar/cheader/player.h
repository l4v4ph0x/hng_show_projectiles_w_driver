#ifndef PLAYER_H
#define PLAYER_H

class player {
public:
  unsigned long processId;

  unsigned long id;
  float x, y, z;
  float look_x, look_y;
  unsigned short hp;

  double degreeFromLocalPlayer;
  double hyp;
  unsigned char side;
  bool visible;
  bool dead;
  unsigned long status;

public:
  player(unsigned long pid, unsigned long base);
  void update();
};

#endif