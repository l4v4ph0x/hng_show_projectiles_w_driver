/*
Copyright (C) 2018 lava phox

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <windows.h>
#include <stdio.h>
#include <vector>
#include <dwmapi.h>
#include "../GL/glut.h"
#include "../GL/freeglut.h"

#include "../cheader/csummoner.h"
#include "../cheader/offsets.h"
#include "../cheader/player.h"


// disable funking _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#define PLAYERS_ARR_SIZE 500

// useless args for glut
int _argc;
char **_argv;

char oldJumptoFunPlayers[6];
char oldJumptoFunLocal[7];

unsigned char jumpToDetourFunPlayers[7] = "\xE8\x00\x00\x00\x00\x90";
unsigned char jumpToDetourFunSwingX[6] = "\xE8\x00\x00\x00\x00";

lplayer *localPlayer;
std::vector<lplayer *> players;


void drawPoint(float x, float y, float size, float r, float g, float b) {
  glColor3f(r, g, b); // Red
  glVertex2f(x - (size / 2), y - (size / 2));    // x, y
  glVertex2f(x + (size / 2), y - (size / 2));
  glVertex2f(x + (size / 2), y + (size / 2));
  glVertex2f(x - (size / 2), y + (size / 2));
}

/*
  * Handler for window-repaint event. Call back when the window first appears and
  * whenever the window needs to be re-painted.
  */
void display() {
  glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Set background color to black and opaque
  glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer (background)

  // Draw a Red 1x1 Square centered at origin
  glBegin(GL_QUADS);              // Each set of 4 vertices form a quad

  if (localPlayer) {
    drawPoint(0, 0, 0.025, 1, 1, 1);

    unsigned int size = players.size();

    for (unsigned int i = 0; i < size; i++) {
      double adjance = 0, opposite = 0;

      if (players[i]->degreeFromLocalPlayer >= 270) {
        double cosed = cos(radians(players[i]->degreeFromLocalPlayer - 270));
        double sined = sin(radians(players[i]->degreeFromLocalPlayer - 270));

        adjance = cosed * players[i]->hyp;
        opposite = sined * players[i]->hyp;

        adjance *= -1;
      } else if (players[i]->degreeFromLocalPlayer >= 180) {
        double cosed = cos(radians(players[i]->degreeFromLocalPlayer - 180));
        double sined = sin(radians(players[i]->degreeFromLocalPlayer - 180));

        adjance = sined * players[i]->hyp;
        opposite = cosed * players[i]->hyp;

        adjance *= -1;
        opposite *= -1;
      } else if (players[i]->degreeFromLocalPlayer >= 90) {
        double cosed =
            cos(radians(players[i]->degreeFromLocalPlayer - 90));
        double sined =
            sin(radians(players[i]->degreeFromLocalPlayer - 90));

        adjance = cosed * players[i]->hyp;
        opposite = sined * players[i]->hyp;

        opposite *= -1;
      } else if (players[i]->degreeFromLocalPlayer >= 0) {
        double cosed = cos(radians(players[i]->degreeFromLocalPlayer));
        double sined = sin(radians(players[i]->degreeFromLocalPlayer));

        adjance = sined * players[i]->hyp;
        opposite = cosed * players[i]->hyp;
      }

      float x = adjance / 200;
      float z = opposite / 200;

      if (players[i]->p.side == localPlayer->p.side) {
        // draw friendly players
        drawPoint(x, z, 0.025, 0, 0, 1);

        //printf("%0.8f %0.8f \n", x, z);
      } else {
        drawPoint(x, z, 0.025, 1, 0, 0);
      }
    }
  }

  glEnd();

  //glFlush();  // Render now
  glutSwapBuffers();
  glutPostRedisplay();
}


unsigned long __stdcall thread_gui_more(void *) {
  HWND hw_hng;
  HWND hw_radar;
  RECT r;

  for (; !hw_hng; Sleep(100)) {
    hw_hng = FindWindowA(0, "H&G");
  }

  for (; !hw_radar; Sleep(100)) {
    hw_radar = FindWindowA(0, "OpenGL Setup Test");
  }

  DWM_BLURBEHIND bb = {0};
  bb.dwFlags = DWM_BB_ENABLE;
  bb.fEnable = true;
  bb.hRgnBlur = nullptr;
  DwmEnableBlurBehindWindow(hw_radar, &bb);

  for (;; Sleep(100)) {
    GetWindowRect(hw_hng, &r);
    SetWindowPos(hw_radar, HWND_TOPMOST, r.right - 320 - 30, r.top + 50, 0, 0, SWP_NOSIZE);

    if (get_proc_light("hng.exe") == 0) {
      ExitProcess(0);
    }
  }


  return 0;
}

unsigned long __stdcall thread_gui(void *) {
  glutInit(&_argc, _argv);
  glutInitDisplayMode(GLUT_CAPTIONLESS);

  glutCreateWindow("OpenGL Setup Test");

  glutInitWindowSize(320, 320);
  glutDisplayFunc(display);
  glutMainLoop();

  return 0;
}

__declspec(naked) void getPlayersDetourFun() {
  __asm {
	push eax // esp + 32
	push ebx // esp + 28
	push ecx // esp + 24
	push edx // esp + 20
	push edi // esp + 16
	push esi // esp + 12

	// addresses that are given when injecting

	// esp + 8 will be getPlayersFun length
	mov esi, 0
	push esi

	// esp + 4 will be player.dll address
	mov esi, 0
	push esi

	// esp  will be globals.dll address
	mov esi, 0
	push esi

	// 0x74E10000 is kernel32.dll
	// 0x778B0000 is ntdll.dll
	// 0x74E24F20 is kernel32 dll get process heap
	// 0x778F4800 is nt dll heap alloc

  // store this function args to registers
	//pop eax // globalsDll
	//pop ecx // playerDll
	//pop ebx //


  mov eax, [esp]
  mov eax, [eax + OFFSET_TERMINAL]
  //add eax, OFFSET_ADDR_PLAYERS

  cmp dword ptr [eax + OFFSET_ADDR_ALLOCATED_FUN], 0xFFFFFFFF
  jne allocatedAlready

  // test to write address we can control user side
  //mov dword ptr [eax + OFFSET_ADDR_ALLOCATED_FUN], 0xDEADBEEF


  // allocate memory where to write players collect function
  mov eax, [esp + 4]
  push PAGE_EXECUTE_READWRITE
  push MEM_COMMIT
  push 2048
  push 0
  call [eax + OFFSET_VIRTUAL_ALLOC]

  mov ecx, [esp]
  mov ecx, [ecx + OFFSET_TERMINAL]
  mov [ecx + OFFSET_ADDR_ALLOCATED_FUN], eax
  jmp funNotInitialized

  allocatedAlready:

  mov eax, [esp]
  mov eax, [eax + OFFSET_TERMINAL]
  mov eax, [eax + OFFSET_ADDR_ALLOCATED_FUN]
  cmp byte ptr [eax], 0
  je funNotInitialized

  mov edi, [esp + 16]
  mov ecx, [esp + 4]
  mov ebx, [esp]

  // call c++ function getPlayersFun
	push byte ptr ss:[ebp - OFFSET_PLAYER_TYPE_EBP]
	push edi // edi
	push ecx // playerDll
	push ebx // globalsDll
	call eax

  funNotInitialized:


	// pop 3 vars we passed first
	pop esi
	pop esi
	pop esi

	pop esi
	pop edi
	pop edx
	pop ecx
	pop ebx
	pop eax

	MOV EAX, DWORD PTR DS:[EDI + OFFSET_ARR1]

	ret
  }
}

unsigned long __stdcall getPlayersFun(unsigned long globalsDll, unsigned long playerDll, unsigned long base, bool playerType) {
	unsigned long *staticAddr = (unsigned long *)(*(unsigned long *)(globalsDll + OFFSET_TERMINAL) + OFFSET_ADDR_PLAYERS);
	unsigned long staticHeapAlloc = playerDll + OFFSET_HEAP_ALLOC;
	unsigned long staticGetProcessHeap = playerDll + OFFSET_GET_PROCESS_HEAP;
	unsigned long heapedAddr = 0;
	unsigned long heapSize = sizeof(player) * PLAYERS_ARR_SIZE;
	unsigned long playersAddr = *staticAddr;
	unsigned long arr1;
	unsigned long arr2;

	// check if we have address to store players
	if (*staticAddr == 0xFFFFFFFF) {
		// if not then
		// allocate memory
		__asm {
			mov eax, staticGetProcessHeap
			call [eax]

			push heapSize
      // HEAP_ZERO_MEMORY = 0x00000008
			push HEAP_ZERO_MEMORY
			push eax
			mov eax, staticHeapAlloc
			call [eax]

			mov heapedAddr, eax
		}

		*staticAddr = heapedAddr;
	} else {
		if (base == 0) {
			return 0;
		}

		arr1 = *(unsigned long *)(base + OFFSET_ARR1);
		arr2 = *(unsigned long *)(base + OFFSET_ARR2);

		if (arr1 == 0 || *(unsigned short *)(arr1 + OFFSET_HP) == 0) {
			return 0;
		}

		for (unsigned long i = 0; i < heapSize; i += sizeof(player)) {
			if ((*(player *)(playersAddr + i)).id == 0) {
				(*(player *)(playersAddr + i)).counter = 10;
				(*(player *)(playersAddr + i)).id = base;
				(*(player *)(playersAddr + i)).isLocalPlayer = playerType;
				(*(player *)(playersAddr + i)).x = *(float *)(arr2 + OFFSET_X2);
				(*(player *)(playersAddr + i)).y = *(float *)(arr2 + OFFSET_X2 +4);
				(*(player *)(playersAddr + i)).z = *(float *)(arr2 + OFFSET_X2 +8);
				(*(player *)(playersAddr + i)).look_x = *(float *)(arr1 + OFFSET_LOOK_X);
				(*(player *)(playersAddr + i)).look_y = *(float *)(arr1 + OFFSET_LOOK_X +4);
				(*(player *)(playersAddr + i)).side =  *(unsigned char *)(arr1 + OFFSET_SIDE);
				(*(player *)(playersAddr + i)).hp =  *(short *)(arr1 + OFFSET_HP);

				break;
			} else if ((*(player *)(playersAddr + i)).id == base) {
				(*(player *)(playersAddr + i)).counter = 10;
				(*(player *)(playersAddr + i)).isLocalPlayer = playerType;
				(*(player *)(playersAddr + i)).x = *(float *)(arr2 + OFFSET_X2);
				(*(player *)(playersAddr + i)).y = *(float *)(arr2 + OFFSET_X2 +4);
				(*(player *)(playersAddr + i)).z = *(float *)(arr2 + OFFSET_X2 +8);
				(*(player *)(playersAddr + i)).look_x = *(float *)(arr1 + OFFSET_LOOK_X);
				(*(player *)(playersAddr + i)).look_y = *(float *)(arr1 + OFFSET_LOOK_X +4);
				(*(player *)(playersAddr + i)).side =  *(unsigned char *)(arr1 + OFFSET_SIDE);
				(*(player *)(playersAddr + i)).hp =  *(short *)(arr1 + OFFSET_HP);

				break;
			}
		}
	}


	return 0;
}


void main(int argc, char **argv) {
  unsigned long terminal;
  unsigned long pid;
  unsigned long playerDll = 0;
  unsigned long globalsDll = 0;

  bool boWroteGetPlayersFun = false;

  _argc = argc;
  _argv = argv;

  printf("waiting for something\n");
  pid = get_proc(L"hng.exe");

  printf("searching modules ... ");
  for (; playerDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&playerDll, L"player.dll");
  }
  for (; globalsDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&globalsDll, L"globals.dll");
  }

  if (playerDll != 0 && globalsDll != 0) {
    printf("| Done\n");


    // read old bytes to change back when exit
    keReadVirtualMemory(pid, playerDll + OFFSET_PLAYERS_TO_DETOUR, (unsigned long)&oldJumptoFunPlayers, 6);

	  unsigned long getPlayersFunDetourLen = (unsigned long)&getPlayersFun - (unsigned long)&getPlayersDetourFun;
    unsigned long getPlayersFunLen = (unsigned long)&main - (unsigned long)&getPlayersFun;

    unsigned long getPlayersFunDetourAddr = playerDll + OFFSET_DETOUR_FUNC_PLAYERS;
    unsigned long getPlayersFunAddr = getPlayersFunDetourAddr + getPlayersFunDetourLen;

    *(unsigned long *)(jumpToDetourFunPlayers + 1) = OFFSET_DETOUR_FUNC_PLAYERS - OFFSET_PLAYERS_TO_DETOUR - 5;


    keWriteProtectedMemory(pid, getPlayersFunDetourAddr,
                           (unsigned long)&getPlayersDetourFun, getPlayersFunDetourLen,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);


     // add params to getPlayersDetourFun
   	keWriteProtectedMemory(pid, getPlayersFunDetourAddr + 7,
                              (unsigned long)&getPlayersFunLen, 4,
                              PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
   	keWriteProtectedMemory(pid, getPlayersFunDetourAddr + 0xD,
                              (unsigned long)&playerDll, 4,
                              PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
   	keWriteProtectedMemory(pid, getPlayersFunDetourAddr + 0x13,
                              (unsigned long)&globalsDll, 4,
                              PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    //printf("%08X\n", playerDll + OFFSET_PLAYERS_TO_DETOUR);

    // jump to get players detour fun
    keWriteProtectedMemory(pid, playerDll + OFFSET_PLAYERS_TO_DETOUR,
                           (unsigned long)jumpToDetourFunPlayers, 6,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);


    //keReadVirtualMemory(pid, playerDll + OFFSET_PLAYERS_TO_DETOUR, (unsigned long)&oldJumptoFunPlayers, 6);
    //for (int i = 0; i < 6; i++) {
    //  printf("%02X\n", oldJumptoFunPlayers[i]);
    //}
    //printf("\n");

    for (; !boWroteGetPlayersFun; Sleep(1000)) {
      keReadVirtualMemory(pid, globalsDll + OFFSET_TERMINAL, (unsigned long)&terminal, 4);

      unsigned long allocatedFunAddr;
      keReadVirtualMemory(pid, terminal + OFFSET_ADDR_ALLOCATED_FUN, (unsigned long)&allocatedFunAddr, 4);
      //printf("%08X\n", allocatedFunAddr);

      if (allocatedFunAddr != 0xFFFFFFFF) {
        // write players gathering fun
        keWriteProtectedMemory(pid, allocatedFunAddr,
                               (unsigned long)&getPlayersFun, getPlayersFunLen,
                               PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

        boWroteGetPlayersFun = true;
      }

    }


    CreateThread(0, 0, thread_gui, 0, 0, 0);
    CreateThread(0, 0, thread_gui_more, 0, 0, 0);

    for (;; Sleep(10)) {
  		players.clear();
  		keReadVirtualMemory(pid, globalsDll + OFFSET_TERMINAL, (unsigned long)&terminal, 4);

  		unsigned long buf;
  		keReadVirtualMemory(pid, terminal + OFFSET_ADDR_PLAYERS, (unsigned long)&buf, 4);

      //printf("ff: %08X\n", buf);

      if (buf != 0xFFFFFFFF) {
  			unsigned char buf2[sizeof(player) * PLAYERS_ARR_SIZE];
  			keReadVirtualMemory(pid, buf, (unsigned long)&buf2, sizeof(player) * PLAYERS_ARR_SIZE);

  			unsigned long nuller = 0;

  			for (unsigned long i = 0; i < sizeof(player) * PLAYERS_ARR_SIZE; i += sizeof(player)) {
  				player p = *(player *)(buf2 + i);
  				if (p.id != 0) {
  					if (p.counter <= 0) {
  						// tell we got expired player
  						// +4 is id, first is counter
  						keWriteVirtualMemory(pid, buf + i +4, (unsigned long)&nuller, 4);
  					} else {
  						// tell that we got it, and we get 5 times to loop when it goes clear
  						p.counter -= 1;
  						keWriteVirtualMemory(pid, buf + i, (unsigned long)&p.counter, 4);

  						lplayer *lp = new lplayer(p);

  						if (p.isLocalPlayer) {
  							delete localPlayer;
  							localPlayer = lp;



  							//printf("look_y: %0.8f %0.8f\n", localPlayer->look_y, localPlayer->p.look_y);
                //printf("pos: %0.8f %0.8f\n", localPlayer->p.x, localPlayer->p.z);
  						} else {
  							players.push_back(lp);
  						}

  						//printf("\t[%08X]: %08X local: %s %.6f %.6f %.6f\n",
  						//	buf + i *4, p.id, p.isLocalPlayer ? "true" : "false", p.x, p.y, p.z);
  					}
  				}
  			}

  			//check_aimbot();
  		} else {
  		}


  	}
  }

  system("pause");
}
