#include "../cheader/player.h"

#include "../cheader/offsets.h"
#include "../cheader/csummoner.h"
#include "../cheader/oldDays.h"

#include <cmath>
#include <stdio.h>

extern lplayer *localPlayer;

double subber = 1.15;

lplayer::lplayer(player p) {
	this->p = p;
	
	look_x = hngAimToDegree(getOldDaysAim(p.look_x));
	if (look_x < 0) {
		look_x = 360 - abs(look_x);
	}

	look_y = hngAimToDegreeY(p.look_y);

	if (localPlayer) {
		double adjance = std::abs(localPlayer->p.z - p.z);
		double opposite = std::abs(localPlayer->p.x - p.x);
		double adjanceY = std::abs(localPlayer->p.y - p.y);

		if (p.x < localPlayer->p.x && p.z < localPlayer->p.z) {
			degreeFromLocalPlayer = 360 - degrees(atan2(opposite, adjance));
		} else if (p.x < localPlayer->p.x && p.z > localPlayer->p.z) {
			degreeFromLocalPlayer = 270 - degrees(atan2(adjance, opposite));
		} else if (p.x > localPlayer->p.x && p.z > localPlayer->p.z) {
			degreeFromLocalPlayer = 90 + degrees(atan2(adjance, opposite));
		} else if (p.x > localPlayer->p.x && p.z < localPlayer->p.z) {
			degreeFromLocalPlayer = degrees(atan2(opposite, adjance));
		}

		float localPlayerDegreeX = localPlayer->look_x - 180;
		if (localPlayerDegreeX < 0) {
			localPlayerDegreeX += 360;
		} else if (localPlayerDegreeX > 360) {
			localPlayerDegreeX -= 360;
		}

		// printf("%f\n", localPlayerDegreeX);

		degreeFromLocalPlayer -= localPlayerDegreeX;
		if (degreeFromLocalPlayer < 0) {
			degreeFromLocalPlayer += 360;
		}
		
		if (GetAsyncKeyState(VK_UP) & 1) {
			subber += 0.01;
			printf("subber: %0.3f\n", subber);
		} else if (GetAsyncKeyState(VK_DOWN) & 1) {
			subber -= 0.01;
			printf("subber: %0.3f\n", subber);
		}
		
		hyp = std::sqrt((adjance * adjance) + (opposite * opposite));
		degreeFromLocalPlayerY = degrees(std::atan(adjanceY / hyp)) * subber;
		//printf("tan(%f / %f) = %f\n", adjanceY, hyp, degreeFromLocalPlayerY);
		if (p.y < localPlayer->p.y) {
			// player higher than me
			degreeFromLocalPlayerY *= -1;
		} else {
			// player below me
			
		}
		
		degreeFromLocalPlayerY -= localPlayer->look_y;
	}
}
