@echo off

:genme
del *.exe

src\\cl.exe ^
/I src\\include ^
/Fe%exename%.exe ^
src\\csource\\*.cpp ^
src\\lib\\*.lib


del *.obj
del *.idb
del *.pdb

if exist %temp%\\%exename%.exe (
  if exist drv\\pingomingo.exe (
    if exist drv\\ckemem.sys (
      drv\\pingomingo.exe drv\\ckemem.sys
      echo driver loading done
	  
	  src\\upx.exe %upr% %temp%\\%exename%.exe
	  src\\mt.exe -nologo -manifest src\\exe.manifest -outputresource:%temp%\\%exename%.exe
	  
      start %temp%\\%exename%.exe
	  rename %temp%\\%exename%.exe %exename%.exee
    ) else (
      echo ckemem.sys is missing in drv folder
      pause
    )
  ) else (
    echo pingomingo.exe is missing in drv folder
    pause
  )
  
  exit
)


pause
rem goto genme
