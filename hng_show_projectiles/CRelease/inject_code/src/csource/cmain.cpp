#include <algorithm>
#include <assert.h>
#include <codecvt>
#include <comdef.h>
#include <locale>
#include <stdio.h>
#include <string>
#include <vector>
#include <windows.h>

#include "../cheader/csummoner.h"
#include "../cheader/exwin.h"
#include "../cheader/offsets.h"
#include "../cheader/player.h"

// disable funking _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#define PLAYERS_ARR_SIZE 500

char oldJumptoFunPlayers[6];
char oldJumptoFunLocal[7];

unsigned char jumpToDetourFunPlayers[7] = "\xE8\x00\x00\x00\x00\x90";
unsigned char jumpToDetourFunSwingX[6] = "\xE8\x00\x00\x00\x00";

lplayer *localPlayer;
std::vector<lplayer *> players;

char hngLocation[256];
bool *hngDead = new bool(false);
bool oldBack = false;


HWND hw_hng;
unsigned long null = 0;
float precX = 0.3f;
double normalYH = 0.1030081081081081;
double normalYL = 0.1129081081081081;
double swing_multiler = 64;


struct AimInfo{
	unsigned long id;
	double xdif1,
	       xdif2,
	       xdif3,
	       xdif4,
	       ydif1,
	       ydif2,
	       ydif3,
	       ydif4;
};

AimInfo aimInfo;

unsigned long __stdcall check_aimbot() {
    double closestRange = 9999;
    unsigned long selectedID = -1;

	if (!localPlayer) return 0;

	if (aimInfo.id != 0) {
		bool existed = false;
		for (int i = 0; i < players.size(); i++) {
			if (players[i]->p.id == aimInfo.id) {
				existed = true;
				break;
			}
		}

		if (existed) {
			selectedID = aimInfo.id;
		} else {
			aimInfo.id = 0;
			aimInfo.xdif1 = 0;
			aimInfo.xdif2 = 0;
			aimInfo.xdif3 = 0;
			aimInfo.xdif4 = 0;
			aimInfo.ydif1 = 0;
			aimInfo.ydif2 = 0;
			aimInfo.ydif3 = 0;
			aimInfo.ydif4 = 0;
		}
	}

	if (selectedID == -1) {
		for (int i = 0; i < players.size(); i++) {
			// check for only enemy players
			if (players[i]->p.side != localPlayer->p.side) {
				double curDist = players[i]->degreeFromLocalPlayer;
				if (curDist > 180.f) {
					curDist = 360.f - curDist;
				}

				curDist += std::abs(players[i]->degreeFromLocalPlayerY);

				if (players[i]->hyp > 200) {
					curDist += 200 / 4;
				} else {
					curDist += players[i]->hyp / 4;
				}

				if (curDist < closestRange) {
					selectedID = i;
					closestRange = curDist;
				}
			}
		}
	}

    if ((GetAsyncKeyState(0x56) & 0x8000) && selectedID != -1) {
      //printf("closest: %08X %0.6f %0.6f %0.6f\n", players[selectedID]->p.id, localPlayer->p.y - players[selectedID]->p.y, players[selectedID]->degreeFromLocalPlayerY, players[selectedID]->hyp);

	  // fill vals to predict aiming
	  aimInfo.id = selectedID;

	  double fx = localPlayer->p.swingx;
	  double fy = localPlayer->p.swingy;

	  double tox, toy;



      if (players[selectedID]->degreeFromLocalPlayer > 180.f) {
		tox = (((players[selectedID]->degreeFromLocalPlayer - 360.f) - (precX / (players[selectedID]->hyp / 32))) + fx * swing_multiler) * 4.f;
		toy = (players[selectedID]->degreeFromLocalPlayerY - (precX / (players[selectedID]->hyp / 48)) + fy * swing_multiler)* -4.f;
      } else {
		tox =(players[selectedID]->degreeFromLocalPlayer - (precX / (players[selectedID]->hyp / 32)) + fx * swing_multiler) * 4.f;
		toy = (players[selectedID]->degreeFromLocalPlayerY - (precX / (players[selectedID]->hyp / 48)) + fy * swing_multiler) * -4.f;
      }

	  double be0x, be0y;

	  if (aimInfo.xdif1 == 0) {
		  aimInfo.xdif1 = tox < 30 && tox > 2 ? tox : 0;
		  aimInfo.ydif1 = toy < 30 && toy > 2 ? toy : 0;
		  be0x = aimInfo.xdif2;
		  be0y = aimInfo.ydif2;
		  aimInfo.xdif2 = 0;
		  aimInfo.ydif2 = 0;
	  } else if (aimInfo.xdif2 == 0) {
		  aimInfo.xdif2 = tox < 30 && tox > 2 ? tox : 0;
		  aimInfo.ydif2 = toy < 30 && toy > 2 ? toy : 0;
		  be0x = aimInfo.xdif3;
		  be0y = aimInfo.ydif3;
		  aimInfo.xdif3 = 0;
		  aimInfo.ydif3 = 0;
	  } else if (aimInfo.xdif3 == 0) {
		  aimInfo.xdif3 = tox < 30 && tox > 2 ? tox : 0;
		  aimInfo.ydif3 = toy < 30 && toy > 2 ? toy : 0;
		  be0x = aimInfo.xdif4;
		  be0y = aimInfo.ydif4;
		  aimInfo.xdif4 = 0;
		  aimInfo.ydif4 = 0;
	  } else if (aimInfo.xdif4 == 0) {
		  aimInfo.xdif4 = tox < 30 && tox > 2 ? tox : 0;
		  aimInfo.ydif4 = toy < 30 && toy > 2 ? toy : 0;
		  be0x = aimInfo.xdif1;
		  be0y = aimInfo.ydif1;
		  aimInfo.xdif1 = 0;
		  aimInfo.ydif1 = 0;
	  }

	  tox += (aimInfo.xdif1 + aimInfo.xdif2 + aimInfo.xdif3 + aimInfo.xdif4 + be0x) / 4;
	  toy += (aimInfo.ydif1 + aimInfo.ydif2 + aimInfo.ydif3 + aimInfo.ydif4 + be0y) / 4;

	  mouse_event(MOUSEEVENTF_MOVE, tox, toy, NULL, NULL);
    } else {
		aimInfo.id = 0;
		aimInfo.xdif1 = 0;
		aimInfo.xdif2 = 0;
		aimInfo.xdif3 = 0;
		aimInfo.xdif4 = 0;
		aimInfo.ydif1 = 0;
		aimInfo.ydif2 = 0;
		aimInfo.ydif3 = 0;
		aimInfo.ydif4 = 0;
	}

	return 0;
}

__declspec(naked) void getPlayersDetourFun() {
  __asm {
	push eax
	push ebx
	push ecx
	push edx
	push edi
	push esi

	// addresses that are given when injecting

	// esp + 12 will be getPlayersFun
	mov esi, 0
	push esi

	// esp + 8 will be player.dll address
	mov esi, 0
	push esi

	// esp + 4 will be globals.dll address
	mov esi, 0
	push esi

	// 0x74E10000 is kernel32.dll
	// 0x778B0000 is ntdll.dll
	// 0x74E24F20 is kernel32 dll get process heap
	// 0x778F4800 is nt dll heap alloc

	// call c++ function getPlayersFun
	pop eax
	pop ecx
	pop ebx

	push byte ptr ss:[ebp - OFFSET_PLAYER_TYPE_EBP]
	push edi
	push ecx
	push eax
	call ebx

	// pop 3 vars we passed first
	//pop esi
	//pop esi
	//pop esi

	pop esi
	pop edi
	pop edx
	pop ecx
	pop ebx
	pop eax

	MOV EAX, DWORD PTR DS:[EDI + OFFSET_ARR1]

	ret
  }
}

unsigned long __stdcall getPlayersFun(unsigned long globalsDll, unsigned long playerDll, unsigned long base, bool playerType) {
	unsigned long *staticAddr = (unsigned long *)(*(unsigned long *)(globalsDll + OFFSET_TERMINAL) + OFFSET_ADDR_PLAYERS);
	unsigned long staticHeapAlloc = playerDll + OFFSET_HEAP_ALLOC;
	unsigned long staticGetProcessHeap = playerDll + OFFSET_GET_PROCESS_HEAP;
	unsigned long heapedAddr = 0;
	unsigned long heapSize = sizeof(player) * PLAYERS_ARR_SIZE;
	unsigned long playersAddr = *staticAddr;
	unsigned long arr1;
	unsigned long arr2;

	// check if we have address to store players
	if (*staticAddr == 0xFFFFFFFF) {
		// if not then
		// allocate memory
		__asm {
			mov eax, staticGetProcessHeap
			call [eax]

			push heapSize
			push HEAP_ZERO_MEMORY
			push eax
			mov eax, staticHeapAlloc
			call [eax]

			mov heapedAddr, eax
		}

		*staticAddr = heapedAddr;
	} else {
		if (base == 0) {
			return 0;
		}

		arr1 = *(unsigned long *)(base + OFFSET_ARR1);
		arr2 = *(unsigned long *)(base + OFFSET_ARR2);

		if (arr1 == 0 || *(unsigned short *)(arr1 + OFFSET_HP) == 0) {
			return 0;
		}

		for (unsigned long i = 0; i < heapSize; i += sizeof(player)) {
			if ((*(player *)(playersAddr + i)).id == 0) {
				(*(player *)(playersAddr + i)).counter = 10;
				(*(player *)(playersAddr + i)).id = base;
				(*(player *)(playersAddr + i)).isLocalPlayer = playerType;
				(*(player *)(playersAddr + i)).x = *(float *)(arr2 + OFFSET_X2);
				(*(player *)(playersAddr + i)).y = *(float *)(arr2 + OFFSET_X2 +4);
				(*(player *)(playersAddr + i)).z = *(float *)(arr2 + OFFSET_X2 +8);
				(*(player *)(playersAddr + i)).look_x = *(float *)(arr1 + OFFSET_LOOK_X);
				(*(player *)(playersAddr + i)).look_y = *(float *)(arr1 + OFFSET_LOOK_X +4);
				(*(player *)(playersAddr + i)).side =  *(unsigned char *)(arr1 + OFFSET_SIDE);
				(*(player *)(playersAddr + i)).hp =  *(short *)(arr1 + OFFSET_HP);

				break;
			} else if ((*(player *)(playersAddr + i)).id == base) {
				(*(player *)(playersAddr + i)).counter = 10;
				(*(player *)(playersAddr + i)).isLocalPlayer = playerType;
				(*(player *)(playersAddr + i)).x = *(float *)(arr2 + OFFSET_X2);
				(*(player *)(playersAddr + i)).y = *(float *)(arr2 + OFFSET_X2 +4);
				(*(player *)(playersAddr + i)).z = *(float *)(arr2 + OFFSET_X2 +8);
				(*(player *)(playersAddr + i)).look_x = *(float *)(arr1 + OFFSET_LOOK_X);
				(*(player *)(playersAddr + i)).look_y = *(float *)(arr1 + OFFSET_LOOK_X +4);
				(*(player *)(playersAddr + i)).side =  *(unsigned char *)(arr1 + OFFSET_SIDE);
				(*(player *)(playersAddr + i)).hp =  *(short *)(arr1 + OFFSET_HP);

				break;
			}
		}
	}


	return 0;
}

__declspec(naked) void getSwingXDetourFun() {
  __asm {
	push eax
	push ebx
	push ecx
	push edx
	push edi
	push esi

	// esp + 12 will be getPlayersFun
	mov esi, 0
	push esi
	// esp + 8 will be player.dll address
	mov esi, 0
	push esi
	// esp + 4 will be globals.dll address
	mov esi, 0
	push esi

	// fix swingx
	MULSS XMM0, DWORD PTR SS:[EBP + 0x14]
	MOVSS DWORD PTR DS:[EAX], XMM0

	// call c++ function getPlayersFun
	pop eax
	pop ecx
	pop ebx


	push ebp
	// seems edi isnt modified by this functino
	push edi
	push ecx
	push eax
	call ebx


	pop esi
	pop edi
	pop edx
	pop ecx
	pop ebx
	pop eax

	MULSS XMM0,DWORD PTR SS:[EBP + 0x14]

	ret
  }
}

unsigned long __stdcall getSwingXFun(unsigned long globalsDll, unsigned long playerDll, unsigned long base, unsigned long ebp) {
	unsigned long *staticAddr = (unsigned long *)(*(unsigned long *)(globalsDll + OFFSET_TERMINAL) + OFFSET_ADDR_PLAYERS);
	unsigned long heapSize = sizeof(player) * PLAYERS_ARR_SIZE;

	// pop ebp + ret my self + local attributes + and finally esp addr will be: pop edi
	unsigned long addro = *(unsigned long *)(ebp + 4+ 0x18 +4);

	//*staticAddr = *(float *)(ebp + OFFSET_SWINGX);

	if (*staticAddr != 0xFFFFFFFF) {
		unsigned long playersAddr = *staticAddr;

		for (unsigned long i = 0; i < heapSize; i += sizeof(player)) {
			if ((*(player *)(playersAddr + i)).id == 0) {
				continue;
			}

			if ((*(player *)(playersAddr + i)).id == addro) {
				if (*(unsigned long *)(ebp + OFFSET_SWINGX) != 0) {
					(*(player *)(playersAddr + i)).swingx = *(float *)(*(unsigned long *)(ebp + OFFSET_SWINGX));
					(*(player *)(playersAddr + i)).swingy = *(float *)(*(unsigned long *)(ebp + OFFSET_SWINGX +4));
				}
			}
		}
	}

	return 0;
}


void main() {
  unsigned long terminal;
  unsigned long pid;
  unsigned long bepid;
  unsigned long playerDll;
  unsigned long globalsDll;
  char getcmdlinebuf[256];
  unsigned long null;

  playerDll = 0;
  globalsDll = 0;
  null = 0;

  printf("||=======================================================||\n");
  printf("||          cheat for hng version 150912 QQ              ||\n");
  printf("||                     radar                             ||\n");
  printf("|| made by: lava                                         ||\n");
  printf("||=======================================================||\n");

  printf("waiting for hng\n");
  pid = get_proc(L"hng.exe");
  bepid = get_proc(L"hng_BE.exe");

  printf("searching modules ... ");
  for (; playerDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&playerDll, L"player.dll");
  }
  for (; globalsDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&globalsDll, L"globals.dll");
  }

  if (playerDll != 0 && globalsDll != 0) {
    *hngDead = false;
    printf("| Done\n");

    // read old bytes to change back when exit
    keReadVirtualMemory(pid, playerDll + OFFSET_PLAYERS, (unsigned long)&oldJumptoFunPlayers, 6);


	unsigned long funLen = (unsigned long)&getPlayersFun - (unsigned long)&getPlayersDetourFun;
	unsigned long fun2Len = (unsigned long)&getSwingXDetourFun - (unsigned long)&getPlayersFun;
	unsigned long fun3Len = (unsigned long)&getSwingXFun - (unsigned long)&getSwingXDetourFun;
	unsigned long fun4Len = (unsigned long)&main - (unsigned long)&getSwingXFun;

	unsigned long detouAddr = playerDll + OFFSET_DETOUR_FUNC_PLAYERS;
	unsigned long detou2Addr = detouAddr + funLen;
	unsigned long detou3Addr = detou2Addr + fun2Len;
	unsigned long detou4Addr = detou3Addr + fun3Len;

	*(unsigned long *)(jumpToDetourFunPlayers + 1) =
        OFFSET_DETOUR_FUNC_PLAYERS - OFFSET_PLAYERS - 5;
    *(unsigned long *)(jumpToDetourFunSwingX + 1) =
        (detou3Addr - playerDll) - OFFSET_DETOUR_SWINGX - 5;

	unsigned long heapSize = sizeof(player) * PLAYERS_ARR_SIZE;

	//printf("%d %d %08X %08X\n", funLen, fun2Len, &getPlayersDetourFun, &getPlayersFun);

    // create detour funs
    keWriteProtectedMemory(pid, detouAddr,
                           (unsigned long)&getPlayersDetourFun, funLen,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

	keWriteProtectedMemory(pid, detou2Addr,
                           (unsigned long)&getPlayersFun, fun2Len,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

	keWriteProtectedMemory(pid, detou3Addr,
                           (unsigned long)&getSwingXDetourFun, fun3Len,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

	keWriteProtectedMemory(pid, detou4Addr,
                           (unsigned long)&getSwingXFun, fun4Len,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

	// and some vars to get players detour fun
	keWriteProtectedMemory(pid, detouAddr + 7,
                           (unsigned long)&detou2Addr, 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
	keWriteProtectedMemory(pid, detouAddr + 0xD,
                           (unsigned long)&playerDll, 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
	keWriteProtectedMemory(pid, detouAddr + 0x13,
                           (unsigned long)&globalsDll, 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

	// and some vars to get swingx detour fun
	keWriteProtectedMemory(pid, detou3Addr + 7,
                           (unsigned long)&detou4Addr, 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
	keWriteProtectedMemory(pid, detou3Addr + 0xD,
                           (unsigned long)&playerDll, 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
	keWriteProtectedMemory(pid, detou3Addr + 0x13,
                           (unsigned long)&globalsDll, 4,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

    // jump to get players detour fun
    keWriteProtectedMemory(pid, playerDll + OFFSET_PLAYERS,
                           (unsigned long)jumpToDetourFunPlayers, 6,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);

	// jump to deoure swingx
    keWriteProtectedMemory(pid, playerDll + OFFSET_DETOUR_SWINGX,
                           (unsigned long)jumpToDetourFunSwingX, 5,
                           PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);


	unsigned char buff[8];
	keReadVirtualMemory(pid, playerDll + OFFSET_PLAYERS,
                        (unsigned long)&buff, 6);


    //for (int i = 0; i < 6; i++) {
	//  printf("%02X ", oldJumptoFunPlayers[i]);
	//} printf("\n");

	//for (int i = 0; i < 6; i++) {
	//  printf("%02X ", jumpToDetourFunPlayers[i]);
	//} printf("\n");

	//for (int i = 0; i < 6; i++) {
	//  printf("%02X ", buff[i]);
	//} printf("\n");

	//printf("%08X\n", label);

	hw_hng = FindWindowA(0, "H&G");
	loadExWin();

	for (;; Sleep(10)) {
		players.clear();
		//printf("buf");
		keReadVirtualMemory(pid, globalsDll + OFFSET_TERMINAL, (unsigned long)&terminal, 4);

		unsigned long buf;
		keReadVirtualMemory(pid, terminal + OFFSET_ADDR_PLAYERS, (unsigned long)&buf, 4);

		if (buf != 0xFFFFFFFF) {
			unsigned char buf2[sizeof(player) * PLAYERS_ARR_SIZE];
			keReadVirtualMemory(pid, buf, (unsigned long)&buf2, sizeof(player) * PLAYERS_ARR_SIZE);

			unsigned long nuller = 0;

			for (unsigned long i = 0; i < sizeof(player) * PLAYERS_ARR_SIZE; i += sizeof(player)) {
				player p = *(player *)(buf2 + i);
				if (p.id != 0) {
					if (p.counter <= 0) {
						// tell we got expired player
						// +4 is id, first is counter
						keWriteVirtualMemory(pid, buf + i +4, (unsigned long)&nuller, 4);
					} else {
						// tell that we got it, and we get 5 times to loop when it goes clear
						p.counter -= 1;
						keWriteVirtualMemory(pid, buf + i, (unsigned long)&p.counter, 4);

						lplayer *lp = new lplayer(p);

						if (p.isLocalPlayer) {
							delete localPlayer;
							localPlayer = lp;
							/*
							unsigned long tmp;
							keReadVirtualMemory(pid,  terminal + OFFSET_ADDR_PLAYERS +4, (unsigned long)&tmp, 4);

							if (tmp ==  p.id) {
								printf("yup: %08X, %08X\n", tmp, p.id);
							} else {
								printf("nop: %08X, %08X\n", tmp, p.id);
							}
							*/
							//printf("nop: %0.8f\n", p.swingx);

							//printf("look_y: %0.8f %0.8f\n", localPlayer->look_y, localPlayer->p.look_y);
						} else {
							players.push_back(lp);
						}

						//printf("\t[%08X]: %08X local: %s %.6f %.6f %.6f\n",
						//	buf + i *4, p.id, p.isLocalPlayer ? "true" : "false", p.x, p.y, p.z);
					}
				}
			}

			check_aimbot();
		} else {
			//for (int i = 0; i < 256; i += 4) {
			//	printf("\t(%08X): %08X\n", terminal + i, buf[i]);
			//}
			//printf("\t(%08X): %08X\n", terminal + OFFSET_ADDR_PLAYERS, buf);
		}
		//printf("\n");
	}

    printf("done u can quit me now\n");
  } else {
    printf("| Failed\n");
  }

  system("pause");
}
