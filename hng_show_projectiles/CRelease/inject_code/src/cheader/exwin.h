#ifndef EXWIN_H
#define EXWIN_H

#define ID_SHOW_FRIENDLY 0x101
#define ID_SHOW_ENEMY 0x102

#define ID_DOT_SIZE1 0x103
#define ID_DOT_SIZE2 0x104
#define ID_DOT_SIZE3 0x105

void loadExWin();

#endif