#ifndef PLAYER_H
#define PLAYER_H

struct player {
	int counter;
	unsigned long id;
	bool isLocalPlayer;
	float x, y, z;
	float look_x, look_y;
	unsigned char side;
	short hp;
	float swingx, swingy;
};

class lplayer {
	public:
		player p;
		
		float look_x, look_y;
		double degreeFromLocalPlayer;
		double degreeFromLocalPlayerY;
		double hyp;

	public:
		lplayer(player p);
};
#endif