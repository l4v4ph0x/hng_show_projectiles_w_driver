/*
Copyright (C) 2018 lava phox

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef CSUMMONER_H
#define CSUMMONER_H

#include <Windows.h>

unsigned long get_proc_light(const char *name);
unsigned long get_proc(const wchar_t *name);

void keReadVirtualMemory(unsigned long targetPid, unsigned long targetAddress,
                         unsigned long sourceAddress, unsigned long size);

void keWriteVirtualMemory(unsigned long targetPid, unsigned long targetAddress,
                          unsigned long sourceAddress, unsigned long size);

void keGetProcessModule(unsigned long targetPid, unsigned long sourceAddress,
                        const wchar_t *name);

void keWriteProtectedMemory(unsigned long targetPid,
                            unsigned long targetAddress,
                            unsigned long sourceAddress, 
                            unsigned long size,
                            unsigned long protectionBeforeWrite,
                            unsigned long protectionAfterWrite);
                        

#endif
