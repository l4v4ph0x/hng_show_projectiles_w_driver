/*
Copyright (C) 2018 lava phox

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <windows.h>

#include "../cheader/csummoner.h"

// 65C1C570-65BC0000
// 6360C6B0-635B0000
// 5EE7C970 - 5EE20000 = 5C970
const unsigned long OFFSET_TERMINAL = 0x5C970;
const unsigned long OFFSET_SHOW_PROJECTILES = 0x120;
const unsigned long OFFSET_CLEAR_ALL_DEBUG = 0x3BC;

void main() {
  unsigned long terminal;
  unsigned long pid;
  unsigned long globalsDll = 0;
  unsigned char showProjectiles = 0;

  printf("waiting for Services and Controller app\n");
  pid = get_proc(L"hng.exe");


  printf("searching Shell Infrastructure Host ... ");

  for (; globalsDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&globalsDll, L"globals.dll");
  }

  if (globalsDll != 0) {
    printf("- Done\n");
    printf("enabling System interrupts ... ");

    keReadVirtualMemory(pid, globalsDll + OFFSET_TERMINAL,
                        (unsigned long)&terminal, 4);

    keWriteVirtualMemory(pid, terminal + OFFSET_SHOW_PROJECTILES,
                         (unsigned long)"\x01", 1);

    keReadVirtualMemory(pid, terminal + OFFSET_SHOW_PROJECTILES,
                        (unsigned long)&showProjectiles, 1);

    if (showProjectiles != 0) {
      printf("- Done\n");
      printf("wokring to Start-Up Application every 0.5s ... ");

      for (; pid != 0; Sleep(525)) {
        pid = get_proc_light("hng.exe");

        if (pid != 0) {
          keWriteVirtualMemory(pid, terminal + OFFSET_CLEAR_ALL_DEBUG,
                               (unsigned long)"\x01", 1);
        }
      }
      printf("\nseems like Client Server Runtime Process ended\n");
    } else {
      printf("- Failed\n");
    }
  } else {
    printf("- Failed\n");
  }

  system("pause");
}
