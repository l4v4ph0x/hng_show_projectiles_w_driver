@echo off
setlocal EnableDelayedExpansion

:genme
del %temp%\\*.exee

set /A rando=%RANDOM% * 9 / 32768
set /A gen=%RANDOM% * 7 / 32768
set /A genm2=%RANDOM% * 12 / 32768

set /A COUNTER=0

for /f "tokens=*" %%a in (exenames.txt) do (
  set /A COUNTER+=1
  set list[!COUNTER!]=%%a
)

set /A randomfixed = 1 + (%RANDOM% * (%COUNTER%-1) / 32767)
set exename=!list[%randomfixed%]!

set /A uprando=%RANDOM% * 12 / 32768

set opti=/O1
set cgen=/
set cgenm2=/
set upr=-1

echo rando: %rando%
if %rando%==1 set opti=/O2
if %rando%==2 set opti=/Od
if %rando%==3 set opti=/Oi
if %rando%==4 set opti=/Os
if %rando%==5 set opti=/Ot
if %rando%==6 set opti=/Ox
if %rando%==7 set opti=/Oy
if %rando%==8 set opti=/favor:blend

echo gen: %gen%
if %gen%==0 set cgen=/clr:nostdlib
if %gen%==1 set cgen=/EHsc
if %gen%==2 set cgen=/arch:IA32
if %gen%==3 set cgen=/arch:SSE
if %gen%==4 set cgen=/arch:SSE2
if %gen%==5 set cgen=/arch:AVX
if %gen%==6 set cgen=/arch:AVX2

echo genm2: %genm2%
if %genm2%==0 set cgenm2=/Gd
if %genm2%==1 set cgenm2=/Gr
if %genm2%==2 set cgenm2=/Gv
if %genm2%==3 set cgenm2=/Gz
if %genm2%==4 set cgenm2=/Gm /Zi
if %genm2%==5 set cgenm2=/GS
if %genm2%==6 set cgenm2=/Gy
if %genm2%==7 set cgenm2=/Gz
if %genm2%==8 set cgenm2=/RTC1
if %genm2%==9 set cgenm2=/RTCc
if %genm2%==10 set cgenm2=/RTCs
if %genm2%==11 set cgenm2=/RTCu

echo uprando: %uprando%
if %uprando%==1 set upr=-2
if %uprando%==2 set upr=-3
if %uprando%==3 set upr=-4
if %uprando%==4 set upr=-5
if %uprando%==5 set upr=-6
if %uprando%==6 set upr=-7
if %uprando%==7 set upr=-8
if %uprando%==8 set upr=-9
if %uprando%==9 set upr=--best
if %uprando%==10 set upr=--brute
if %uprando%==11 set upr=--ultra-brute

src\\cl.exe %cgen% %cgenm2% /MT /Zl %opti% ^
/I src\\include ^
/Fe%temp%\\%exename%.exe ^
src\\csource\\*.cpp ^
src\\lib\\*.lib

del *.obj
del *.idb
del *.pdb

if exist %temp%\\%exename%.exe (
  if exist drv\\pingomingo.exe (
    if exist drv\\ckemem.sys (
      drv\\pingomingo.exe drv\\ckemem.sys
      echo driver loading done
	  
	  src\\upx.exe %upr% %temp%\\%exename%.exe
	  src\\mt.exe -nologo -manifest src\\exe.manifest -outputresource:%temp%\\%exename%.exe
	  
      start %temp%\\%exename%.exe
	  rename %temp%\\%exename%.exe %exename%.exee
    ) else (
      echo ckemem.sys is missing in drv folder
      pause
    )
  ) else (
    echo pingomingo.exe is missing in drv folder
    pause
  )
  
  exit
)

goto genme
