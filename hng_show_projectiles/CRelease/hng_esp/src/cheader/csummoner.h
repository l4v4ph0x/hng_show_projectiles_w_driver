#ifndef CSUMMONER_H
#define CSUMMONER_H

#include <Windows.h>

unsigned long get_module(unsigned long pid, const char *module_name);
unsigned long get_proc_light(const char *name);
unsigned long get_proc(const wchar_t *name);

void keReadVirtualMemory(unsigned long targetPid, unsigned long targetAddress,
                         unsigned long sourceAddress, unsigned long size);

void keWriteVirtualMemory(unsigned long targetPid, unsigned long targetAddress,
                          unsigned long sourceAddress, unsigned long size);

void keGetProcessModule(unsigned long targetPid, unsigned long sourceAddress,
                        const wchar_t *name);

void keWriteProtectedMemory(unsigned long targetPid,
                            unsigned long targetAddress,
                            unsigned long sourceAddress, unsigned long size,
                            unsigned long protectionBeforeWrite,
                            unsigned long protectionAfterWrite);

#endif
