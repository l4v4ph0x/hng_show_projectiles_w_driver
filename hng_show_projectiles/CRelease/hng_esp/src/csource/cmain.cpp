#include <algorithm>
#include <assert.h>
#include <codecvt>
#include <comdef.h>
#include <locale>
#include <stdio.h>
#include <string>
#include <vector>
#include <windows.h>

#include "../cheader/csummoner.h"
#include "../cheader/offsets.h"

// disable funking _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

char hngLocation[256];


void main() {
  unsigned long terminal;
  unsigned long pid;
  unsigned long bepid;
  unsigned long playerDll;
  unsigned long globalsDll;
  char getcmdlinebuf[256];
  unsigned long null;

  playerDll = 0;
  globalsDll = 0;
  null = 0;

  printf("waiting for hng\n");
  pid = get_proc(L"hng.exe");
  bepid = get_proc(L"hng_BE.exe");

  printf("searching modules ... ");
  for (; playerDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&playerDll, L"player.dll");
  }
  for (; globalsDll == 0; Sleep(100)) {
    keGetProcessModule(pid, (unsigned long)&globalsDll, L"globals.dll");
  }

  if (playerDll != 0 && globalsDll != 0) {
    printf("done");



    // create detour funs
    //keWriteProtectedMemory(pid, detouAddr,
    //                       (unsigned long)&getPlayersDetourFun, funLen,
    //                       PAGE_EXECUTE_READWRITE, PAGE_EXECUTE_READWRITE);
						   

    printf("done u can quit me now\n");
  } else {
    printf("| Failed\n");
  }
  
  system("pause");
}
